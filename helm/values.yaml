# SPDX-License-Identifier: EUPL-1.2

## @section Global parameters
## Global Docker image parameters
## Please, note that this will override the image parameters, including dependencies, configured to use the global value
## Current available global Docker image parameters: imageRegistry, imagePullSecrets and storageClass

## @param global.imageRegistry Global Docker image registry
## @param global.imagePullSecrets Global Docker registry secret names as an array
##
global:
  imageRegistry: ""
  ## E.g.
  ## imagePullSecrets:
  ##   - myRegistryKeySecretName
  ##
  imagePullSecrets: []
  ## Compatibility adaptations for Kubernetes platforms
  ##
  compatibility:
    ## Compatibility adaptations for Openshift
    ##
    openshift:
      ## @param global.compatibility.openshift.adaptSecurityContext Adapt the securityContext sections of the deployment to make them compatible with Openshift restricted-v2 SCC: remove runAsUser, runAsGroup and fsGroup and let the platform use their allowed default IDs. Possible values: auto (apply if the detected running cluster is Openshift), force (perform the adaptation always), disabled (do not perform adaptation)
      ##
      adaptSecurityContext: auto

## @section Common parameters

## @param nameOverride String to partially override kimi.fullname template (will maintain the release name)
##
nameOverride: ""
## @param fullnameOverride String to fully override kimi.fullname template
##
fullnameOverride: ""
## @param namespaceOverride String to fully override common.names.namespace
##
namespaceOverride: ""
## @param kubeVersion Force target Kubernetes version (using Helm capabilities if not set)
##
kubeVersion: ""
## @param clusterDomain Kubernetes Cluster Domain
##
clusterDomain: cluster.local
## @param extraDeploy Extra objects to deploy (value evaluated as a template)
##
extraDeploy: []
## @param commonLabels Add labels to all the deployed resources
##
commonLabels: {}
## @param commonAnnotations Add annotations to all the deployed resources
##
commonAnnotations: {}
## Enable diagnostic mode in the deployment
##
diagnosticMode:
  ## @param diagnosticMode.enabled Enable diagnostic mode (all probes will be disabled and the command will be overridden)
  ##
  enabled: false
  ## @param diagnosticMode.command Command to override all containers in the deployment
  ##
  command:
    - sleep
  ## @param diagnosticMode.args Args to override all containers in the deployment
  ##
  args:
    - infinity

## @param automountServiceAccountToken Mount Service Account token in pod
##
automountServiceAccountToken: false
## @param hostAliases Deployment pod host aliases
## https://kubernetes.io/docs/concepts/services-networking/add-entries-to-pod-etc-hosts-with-host-aliases/
##
hostAliases: []
## Command and args for running the container (set to default if not set). Use array form
## @param command Override default container command (useful when using custom images)
## @param args Override default container args (useful when using custom images)
##
command: []
args: []
## @param extraEnvVars Extra environment variables to be set on kimi containers
## E.g:
## extraEnvVars:
##   - name: FOO
##     value: BAR
##
extraEnvVars: []
## @param extraEnvVarsCM ConfigMap with extra environment variables
##
extraEnvVarsCM: ""
## @param extraEnvVarsSecret Secret with extra environment variables
##
extraEnvVarsSecret: ""
## @section kimi deployment parameters

## @param replicaCount Number of kimi replicas to deploy
##
replicaCount: 1
## @param revisionHistoryLimit The number of old history to retain to allow rollback
##
revisionHistoryLimit: 10
## @param updateStrategy.type kimi deployment strategy type
## @param updateStrategy.rollingUpdate kimi deployment rolling update configuration parameters
## ref: https://kubernetes.io/docs/concepts/workloads/controllers/deployment/#strategy
##
updateStrategy:
  type: RollingUpdate
  rollingUpdate: {}
## @param podLabels Additional labels for kimi pods
## ref: https://kubernetes.io/docs/concepts/overview/working-with-objects/labels/
##
podLabels: {}
## @param podAnnotations Annotations for kimi pods
## ref: https://kubernetes.io/docs/concepts/overview/working-with-objects/annotations/
##
podAnnotations: {}
## @param podAffinityPreset Pod affinity preset. Ignored if `affinity` is set. Allowed values: `soft` or `hard`
## ref: https://kubernetes.io/docs/concepts/scheduling-eviction/assign-pod-node/#inter-pod-affinity-and-anti-affinity
##
podAffinityPreset: ""
## @param podAntiAffinityPreset Pod anti-affinity preset. Ignored if `affinity` is set. Allowed values: `soft` or `hard`
## Ref: https://kubernetes.io/docs/concepts/scheduling-eviction/assign-pod-node/#inter-pod-affinity-and-anti-affinity
##
podAntiAffinityPreset: soft
## Node affinity preset
## Ref: https://kubernetes.io/docs/concepts/scheduling-eviction/assign-pod-node/#node-affinity
##
nodeAffinityPreset:
  ## @param nodeAffinityPreset.type Node affinity preset type. Ignored if `affinity` is set. Allowed values: `soft` or `hard`
  ##
  type: ""
  ## @param nodeAffinityPreset.key Node label key to match Ignored if `affinity` is set.
  ## E.g.
  ## key: "kubernetes.io/e2e-az-name"
  ##
  key: ""
  ## @param nodeAffinityPreset.values Node label values to match. Ignored if `affinity` is set.
  ## E.g.
  ## values:
  ##   - e2e-az1
  ##   - e2e-az2
  ##
  values: []
## @param affinity Affinity for pod assignment
## ref: https://kubernetes.io/docs/concepts/configuration/assign-pod-node/#affinity-and-anti-affinity
## Note: podAffinityPreset, podAntiAffinityPreset, and  nodeAffinityPreset will be ignored when it's set
##
affinity: {}
## @param hostNetwork Specify if host network should be enabled for kimi pod
##
hostNetwork: false
## @param hostIPC Specify if host IPC should be enabled for kimi pod
##
hostIPC: false
## DNS-Pod services
## Ref: https://kubernetes.io/docs/concepts/services-networking/dns-pod-service/
## @param dnsPolicy Specifies the DNS policy for the kimi pod
## DNS policies can be set on a per-Pod basis. Currently Kubernetes supports the following Pod-specific DNS policies.
## Available options: Default, ClusterFirst, ClusterFirstWithHostNet, None
## Ref: https://kubernetes.io/docs/concepts/services-networking/dns-pod-service/#pod-s-dns-policy
dnsPolicy: ""
## @param dnsConfig  Allows users more control on the DNS settings for a Pod. Required if `dnsPolicy` is set to `None`
## The dnsConfig field is optional and it can work with any dnsPolicy settings.
## Ref: https://kubernetes.io/docs/concepts/services-networking/dns-pod-service/#pod-dns-config
## E.g.
## dnsConfig:
##   nameservers:
##     - 192.0.2.1 # this is an example
##   searches:
##     - ns1.svc.cluster-domain.example
##     - my.dns.search.suffix
##   options:
##     - name: ndots
##       value: "2"
##     - name: edns0
dnsConfig: {}
## @param nodeSelector Node labels for pod assignment. Evaluated as a template.
## Ref: https://kubernetes.io/docs/concepts/scheduling-eviction/assign-pod-node/
##
nodeSelector: {}
## @param tolerations Tolerations for pod assignment. Evaluated as a template.
## Ref: https://kubernetes.io/docs/concepts/configuration/taint-and-toleration/
##
tolerations: []
## @param priorityClassName kimi pods' priorityClassName
##
priorityClassName: ""
## @param schedulerName Name of the k8s scheduler (other than default)
## ref: https://kubernetes.io/docs/tasks/administer-cluster/configure-multiple-schedulers/
##
schedulerName: ""
## @param terminationGracePeriodSeconds In seconds, time the given to the kimi pod needs to terminate gracefully
## ref: https://kubernetes.io/docs/concepts/workloads/pods/pod/#termination-of-pods
##
terminationGracePeriodSeconds: ""
## @param topologySpreadConstraints Topology Spread Constraints for pod assignment
## https://kubernetes.io/docs/concepts/workloads/pods/pod-topology-spread-constraints/
## The value is evaluated as a template
##
topologySpreadConstraints: []
## kimi pods' Security Context.
## ref: https://kubernetes.io/docs/tasks/configure-pod-container/security-context/#set-the-security-context-for-a-pod
## @param podSecurityContext.enabled Enabled kimi pods' Security Context
## @param podSecurityContext.fsGroupChangePolicy Set filesystem group change policy
## @param podSecurityContext.supplementalGroups Set filesystem extra groups
## @param podSecurityContext.fsGroup Set kimi pod's Security Context fsGroup
## @param podSecurityContext.sysctls sysctl settings of the kimi pods
##
podSecurityContext:
  enabled: true
  fsGroupChangePolicy: Always
  supplementalGroups: []
  fsGroup: 1001
  ## sysctl settings
  ## Example:
  ## sysctls:
  ## - name: net.core.somaxconn
  ##   value: "10000"
  ##
  sysctls: []
## kimi containers' Security Context.
## ref: https://kubernetes.io/docs/tasks/configure-pod-container/security-context/#set-the-security-context-for-a-container
## @param containerSecurityContext.enabled Enabled containers' Security Context
## @param containerSecurityContext.seLinuxOptions [object,nullable] Set SELinux options in container
## @param containerSecurityContext.runAsUser Set containers' Security Context runAsUser
## @param containerSecurityContext.runAsGroup Set containers' Security Context runAsGroup
## @param containerSecurityContext.runAsNonRoot Set container's Security Context runAsNonRoot
## @param containerSecurityContext.privileged Set container's Security Context privileged
## @param containerSecurityContext.readOnlyRootFilesystem Set container's Security Context readOnlyRootFilesystem
## @param containerSecurityContext.allowPrivilegeEscalation Set container's Security Context allowPrivilegeEscalation
## @param containerSecurityContext.capabilities.drop List of capabilities to be dropped
## @param containerSecurityContext.seccompProfile.type Set container's Security Context seccomp profile
##
containerSecurityContext:
  enabled: true
  seLinuxOptions: null
  runAsUser: 1001
  runAsGroup: 1001
  runAsNonRoot: true
  privileged: false
  readOnlyRootFilesystem: true
  allowPrivilegeEscalation: false
  capabilities:
    drop:
      - ALL
  seccompProfile:
    type: RuntimeDefault
## kimi containers' resource requests and limits
## ref: https://kubernetes.io/docs/concepts/configuration/manage-compute-resources-container/
## We usually recommend not to specify default resources and to leave this as a conscious
## choice for the user. This also increases chances charts run on environments with little
## resources, such as Minikube. If you do want to specify resources, uncomment the following
## lines, adjust them as necessary, and remove the curly braces after 'resources:'.
## @param resourcesPreset Set container resources according to one common preset (allowed values: none, nano, micro, small, medium, large, xlarge, 2xlarge). This is ignored if resources is set (resources is recommended for production).
## More information: https://github.com/bitnami/charts/blob/main/bitnami/common/templates/_resources.tpl#L15
##
resourcesPreset: nano
## @param resources Set container requests and limits for different resources like CPU or memory (essential for production workloads)
## Example:
## resources:
##   requests:
##     cpu: 50m
##     memory: 16Mi
##   limits:
##     cpu: 100m
##     memory: 324Mi
##
resources: {}
## kimi containers' lifecycleHooks
## ref: https://kubernetes.io/docs/concepts/containers/container-lifecycle-hooks/
## ref: https://kubernetes.io/docs/tasks/configure-pod-container/attach-handler-lifecycle-event/
## If you do want to specify lifecycleHooks, uncomment the following
## lines, adjust them as necessary, and remove the curly braces on 'lifecycle:{}'.
## @param lifecycleHooks Optional lifecycleHooks for the kimi container
lifecycleHooks: {}
## Example:
## postStart:
##   exec:
##     command: ["/bin/sh", "-c", "echo Hello from the postStart handler > /usr/share/message"]
## Example:
## preStop:
##   exec:
##     command: ["/bin/sleep", "20"]
##     command: ["/bin/sh","-c","kimi -s quit; while killall -0 kimi; do sleep 1; done"]

## kimi containers' startup probe.
## ref: https://kubernetes.io/docs/concepts/workloads/pods/pod-lifecycle/#container-probes
## @param startupProbe.enabled Enable startupProbe
## @param startupProbe.initialDelaySeconds Initial delay seconds for startupProbe
## @param startupProbe.periodSeconds Period seconds for startupProbe
## @param startupProbe.timeoutSeconds Timeout seconds for startupProbe
## @param startupProbe.failureThreshold Failure threshold for startupProbe
## @param startupProbe.successThreshold Success threshold for startupProbe
##
startupProbe:
  enabled: false
  initialDelaySeconds: 30
  timeoutSeconds: 5
  periodSeconds: 10
  failureThreshold: 6
  successThreshold: 1
## Autoscaling parameters
## @param autoscaling.enabled Enable autoscaling for kimi deployment
## @param autoscaling.minReplicas Minimum number of replicas to scale back
## @param autoscaling.maxReplicas Maximum number of replicas to scale out
## @param autoscaling.targetCPU Target CPU utilization percentage
## @param autoscaling.targetMemory Target Memory utilization percentage
##
autoscaling:
  enabled: false
  minReplicas: ""
  maxReplicas: ""
  targetCPU: ""
  targetMemory: ""
## @param extraVolumes Array to add extra volumes
##
extraVolumes: []
## @param extraVolumeMounts Array to add extra mount
##
extraVolumeMounts: []
## Pods Service Account
## ref: https://kubernetes.io/docs/tasks/configure-pod-container/configure-service-account/
##
serviceAccount:
  ## @param serviceAccount.create Enable creation of ServiceAccount for kimi pod
  ##
  create: true
  ## @param serviceAccount.name The name of the ServiceAccount to use.
  ## If not set and create is true, a name is generated using the `common.names.fullname` template
  name: ""
  ## @param serviceAccount.annotations Annotations for service account. Evaluated as a template.
  ## Only used if `create` is `true`.
  ##
  annotations: {}
  ## @param serviceAccount.automountServiceAccountToken Auto-mount the service account token in the pod
  ##
  automountServiceAccountToken: false
## @section Persistence parameters
##
persistence:
  ## @param persistence.enabled Enable data persistence using PVC
  ##
  enabled: true
  ## @param persistence.storageClass PVC Storage Class for data volume
  ## If defined, storageClassName: <storageClass>
  ## If set to "-", storageClassName: "", which disables dynamic provisioning
  ## If undefined (the default) or set to null, no storageClassName spec is
  ##   set, choosing the default provisioner.  (gp2 on AWS, standard on
  ##   GKE, AWS & OpenStack)
  ##
  storageClass: ""
  ## @param persistence.selector Selector to match an existing Persistent Volume
  ## selector:
  ##   matchLabels:
  ##     app: my-app
  ##
  selector: {}
  ## @param persistence.accessModes PVC Access Modes for data volume
  ##
  accessModes:
    - ReadWriteMany
  ## @param persistence.existingClaim Provide an existing PersistentVolumeClaims
  ## The value is evaluated as a template
  ## So, for example, the name can depend on .Release or .Chart
  ##
  existingClaim: ""
  ## @param persistence.mountPath The path the volume will be mounted at
  ## Note: useful when using custom images
  ##
  mountPath: /data
  ## @param persistence.subPath The subdirectory of the volume to mount to
  ## Useful in dev environments and one PV for multiple services
  ##
  subPath: ""
  ## @param persistence.size PVC Storage Request for RabbitMQ data volume
  ## If you change this value, you might have to adjust `rabbitmq.diskFreeLimit` as well
  ##
  size: 256Mi
  ## @param persistence.annotations Persistence annotations. Evaluated as a template
  ## Example:
  ## annotations:
  ##   example.io/disk-volume-type: SSD
  ##
  annotations: {}
  ## @param persistence.labels Persistence labels. Evaluated as a template
  ## Example:
  ## labels:
  ##   app: my-app
  labels: {}
## @param sidecars Sidecar parameters
## e.g:
## sidecars:
##   - name: your-image-name
##     image: your-image
##     imagePullPolicy: Always
##     ports:
##       - name: portname
##         containerPort: 1234
##
sidecars: []
## @param sidecarSingleProcessNamespace Enable sharing the process namespace with sidecars
## This will switch pod.spec.shareProcessNamespace parameter
##
sidecarSingleProcessNamespace: false
## @param initContainers Extra init containers
##
initContainers: []
## Pod Disruption Budget configuration
## ref: https://kubernetes.io/docs/tasks/run-application/configure-pdb/
##
pdb:
  ## @param pdb.create Created a PodDisruptionBudget
  ##
  create: true
  ## @param pdb.minAvailable Min number of pods that must still be available after the eviction.
  ## You can specify an integer or a percentage by setting the value to a string representation of a percentage (eg. "50%"). It will be disabled if set to 0
  ##
  minAvailable: ""
  ## @param pdb.maxUnavailable Max number of pods that can be unavailable after the eviction.
  ## You can specify an integer or a percentage by setting the value to a string representation of a percentage (eg. "50%"). It will be disabled if set to 0. Defaults to `1` if both `pdb.minAvailable` and `pdb.maxUnavailable` are empty.
  ##
  maxUnavailable: ""
## @section Traffic Exposure parameters
## Network Policies
## Ref: https://kubernetes.io/docs/concepts/services-networking/network-policies/
##
networkPolicy:
  ## @param networkPolicy.enabled Specifies whether a NetworkPolicy should be created
  ##
  enabled: true
  ## @param networkPolicy.allowExternal Don't require server label for connections
  ## The Policy model to apply. When set to false, only pods with the correct
  ## server label will have network access to the ports server is listening
  ## on. When true, server will accept connections from any source
  ## (with the correct destination port).
  ##
  allowExternal: true
  ## @param networkPolicy.allowExternalEgress Allow the pod to access any range of port and all destinations.
  ##
  allowExternalEgress: true
  ## @param networkPolicy.extraIngress [array] Add extra ingress rules to the NetworkPolicy
  ## e.g:
  ## extraIngress:
  ##   - ports:
  ##       - port: 1234
  ##     from:
  ##       - podSelector:
  ##           - matchLabels:
  ##               - role: frontend
  ##       - podSelector:
  ##           - matchExpressions:
  ##               - key: role
  ##                 operator: In
  ##                 values:
  ##                   - frontend
  extraIngress: []
  ## @param networkPolicy.extraEgress [array] Add extra ingress rules to the NetworkPolicy (ignored if allowExternalEgress=true)
  ## e.g:
  ## extraEgress:
  ##   - ports:
  ##       - port: 1234
  ##     to:
  ##       - podSelector:
  ##           - matchLabels:
  ##               - role: frontend
  ##       - podSelector:
  ##           - matchExpressions:
  ##               - key: role
  ##                 operator: In
  ##                 values:
  ##                   - frontend
  ##
  extraEgress: []
  ## @param networkPolicy.ingressNSMatchLabels [object] Labels to match to allow traffic from other namespaces
  ## @param networkPolicy.ingressNSPodMatchLabels [object] Pod labels to match to allow traffic from other namespaces
  ##
  ingressNSMatchLabels: {}
  ingressNSPodMatchLabels: {}

amqp:
  host: ""
  port: 5672
  protocol: amqp
  username: guest
  password: guest

s3:
  host: ""
  port: 443
  useSSL: true
  accessKey: ""
  secretKey: ""

## @section kimi parameters

## NLdoc kimi image version
## ref: https://gitlab.com/toegang-voor-iedereen/pdf/
##
## @param stations Array of station configurations
##
stations:
  ## @param stations[].name Name of the station
  ## @param stations[].image.registry Image registry
  ## @param stations[].image.repository Image repository
  ## @skip stations[].image.tag Image tag (immutable tags are recommended)
  ## @param stations[].image.digest Image digest in the format sha256:aa.... 
  ##        Note: If set, this parameter will override the tag.
  ## @param stations[].image.pullPolicy Specify the image pull policy
  ## @param stations[].image.pullSecrets Specify docker-registry secret names as an array
  ## @param stations[].image.debug Set to true to enable debug logs
  ##
  ## Rest of parameters are inherited by the global parameters.
  ##
  "file-document":
    image: &default-station-image
      registry: registry.gitlab.com
      repository: toegang-voor-iedereen/pdf/stations
      tag: 5.10.1
      digest: ""
      ## Specify a imagePullPolicy
      ## Defaults to 'Always' if image tag is 'latest', else set to 'IfNotPresent'
      ## ref: https://kubernetes.io/docs/concepts/containers/images/#pre-pulled-images
      ##
      pullPolicy: IfNotPresent
      ## Optionally specify an array of imagePullSecrets.
      ## Secrets must be manually created in the namespace.
      ## ref: https://kubernetes.io/docs/tasks/configure-pod-container/pull-image-private-registry/
      ## E.g.:
      ## pullSecrets:
      ##   - myRegistryKeySecretName
      ##
      pullSecrets: []
      ## Set to true if you would like to see extra information on logs
      ##
      debug: false
    configuration:
      input:
        type: "file"
      output: 
        type: "document"

  "document-mimetype":
    image: *default-station-image
    configuration:
      input: 
        type: "document"
      workerResult: "attributeWorkerResult"
      output:
        type: "documentAttribute"
        attribute:
          type: "string"
          name: "mimetype"

  'document-metadata':
    image: *default-station-image
    configuration:
      input: 
        type: "document"
        requiredAttributes:
          - name: "mimetype"
            behavior: "await_all"
            requiredValue:
              condition: "equalsString"
              string: "application/pdf"
      workerResult: "documentMetaDataWorkerResult"
      output: 
        type: "documentMetaData"

  'document-pages':
    image: *default-station-image
    configuration:
      input: 
        type: "document"
        requiredAttributes:
          - name: "mimetype"
            behavior: "await_all"
            requiredValue:
              condition: "equalsString"
              string: "application/pdf"
          - name: "pageCount"
            behavior: "await_all"
      workerResult: "pageWorkerResult"
      output:
        type: "page"

  'page-interpreted-content':
    image: *default-station-image
    configuration:
      input: 
        type: "page"
      workerResult: "contentWorkerResult"
      output:
        type: "pageAttribute"
        attribute:
          type: "array"
          name: "interpretedContent"

  'page-regions':
    image: *default-station-image
    configuration:
      input: 
        type: "page"
        requiredAttributes:
          - name: "interpretedContent"
            behavior: "await_all"
      workerResult: "contentWorkerResult"
      output:
        type: "pageAttribute"
        attribute:
          type: "array"
          name: "regions"

  'page-content':
    image: *default-station-image
    configuration:
      input: 
        type: "page"
        requiredAttributes:
          - name: "interpretedContent"
            behavior: "await_all"
          - name: "regions"
            behavior: "await_all"
      workerResult: "contentWorkerResult"
      output:
        type: "pageAttribute"
        attribute:
          type: "array"
          name: "content"

  'pagecollection-documentcontent':
    image: *default-station-image
    configuration:
      input: 
        type: "pageCollection"
        requiredAttributes:
          - name: "content"
            behavior: "await_all"
      workerResult: "contentWorkerResult"
      output:
        type: "documentContent"
        attribute:
          type: "array"
          name: "content"

  'document-output':
    image: *default-station-image
    configuration:
      input: 
        type: "document"
        requiredAttributes:
          - name: "content"
            behavior: "await_all"
      workerResult: "fileWorkerResult"
      output:
        type: "output"
        attribute:
          type: "file"
          name: "html"

## @param workers[].name Name of the worker
## @param workers[].image.registry Image registry
## @param workers[].image.repository Image repository
## @skip workers[].image.tag Image tag (immutable tags are recommended)
## @param workers[].image.digest Image digest in the format sha256:aa.... 
##        Note: If set, this parameter will override the tag.
## @param workers[].image.pullPolicy Specify the image pull policy
## @param workers[].image.pullSecrets Specify docker-registry secret names as an array
## @param workers[].image.debug Set to true to enable debug logs
##
## Rest of parameters are inherited by the global parameters.
##
workers:
  'document-metadata':
    image:
      registry: registry.gitlab.com
      repository: toegang-voor-iedereen/pdf/workers/document-metadata
      tag: 1.14.4
      digest: ""
      pullPolicy: IfNotPresent
      pullSecrets: []
      debug: false
    amqpExchange: document-metadata

  'document-mimetype-from-header':
    image:
      registry: registry.gitlab.com
      repository: toegang-voor-iedereen/pdf/workers/document-mimetype-from-header
      tag: 1.20.1
      digest: ""
      pullPolicy: IfNotPresent
      pullSecrets: []
      debug: false
    amqpExchange: document-mimetype

  'document-output-nldocspec':
    image:
      registry: registry.gitlab.com
      repository: toegang-voor-iedereen/pdf/workers/document-output-nldocspec
      digest: ""
      tag: 3.0.5
      pullPolicy: IfNotPresent
      pullSecrets: []
      debug: false
    amqpExchange: document-output

  'document-pages':
    image:
      registry: registry.gitlab.com
      repository: toegang-voor-iedereen/pdf/workers/document-pages
      tag: 1.13.1
      digest: ""
      pullPolicy: IfNotPresent
      pullSecrets: []
      debug: false
    amqpExchange: document-page

  'page-content':
    image:
      registry: registry.gitlab.com
      repository: toegang-voor-iedereen/pdf/workers/page-content
      tag: 1.18.1
      digest: ""
      pullPolicy: IfNotPresent
      pullSecrets: []
      debug: false
    amqpExchange: page-content

  'page-content-images':
    image:
      registry: registry.gitlab.com
      repository: toegang-voor-iedereen/pdf/workers/page-content-images
      tag: 2.4.5
      digest: ""
      pullPolicy: IfNotPresent
      pullSecrets: []
      debug: false
    amqpExchange: page-content

  'page-content-tables':
    image:
      registry: registry.gitlab.com
      repository: toegang-voor-iedereen/pdf/workers/page-content-tables
      tag: 1.3.2
      digest: ""
      pullPolicy: IfNotPresent
      pullSecrets: []
      debug: false
    amqpExchange: page-content

  'page-interpreted-content-ocr':
    image:
      registry: registry.gitlab.com
      repository: toegang-voor-iedereen/pdf/workers/page-interpreted-content-ocr
      tag: 1.14.3
      digest: ""
      pullPolicy: IfNotPresent
      pullSecrets: []
      debug: false
    amqpExchange: page-interpretedcontent

  'page-regions-image-processing':
    image:
      registry: registry.gitlab.com
      repository: toegang-voor-iedereen/pdf/workers/page-regions-image-processing
      tag: 1.14.5
      digest: ""
      pullPolicy: IfNotPresent
      pullSecrets: []
      debug: false
    amqpExchange: page-regions

  'page-regions-yolo':
    image:
      registry: registry.gitlab.com
      repository: toegang-voor-iedereen/pdf/workers/page-regions-yolo
      tag: 1.17.3
      digest: ""
      pullPolicy: IfNotPresent
      pullSecrets: []
      debug: false
    amqpExchange: page-regions
