{{- /*
SPDX-License-Identifier: EUPL-1.2
*/}}

{{- range $type, $range := (dict "station" .Values.stations "worker" .Values.workers ) }}
{{- range $name, $entity := $range }}

{{- $component := print $type "." $name }}

# component: {{ $component }} type: {{ $type }} name: {{ $name }}

{{- $autoscaling := default $.Values.autoscaling .autoscaling }}
{{- $replicaCount := default $.Values.replicaCount .replicaCount }}
{{- $updateStrategy := default $.Values.updateStrategy .updateStrategy }}
{{- $revisionHistoryLimit := default $.Values.revisionHistoryLimit .revisionHistoryLimit }}
{{- $commonLabels := default $.Values.commonLabels .commonLabels }}
{{- $commonAnnotations := default $.Values.commonAnnotations .commonAnnotations }}
{{- $podLabels := default $.Values.podLabels .podLabels }}
{{- $podAnnotations := default $.Values.podAnnotations .podAnnotations }}
{{- $podAffinityPreset := default $.Values.podAffinityPreset .podAffinityPreset }}
{{- $podAntiAffinityPreset := default $.Values.podAntiAffinityPreset .podAntiAffinityPreset }}
{{- $nodeAffinityPreset := default $.Values.nodeAffinityPreset .nodeAffinityPreset }}
{{- $amqp := default $.Values.amqp .amqp }}
{{- $s3 := default $.Values.s3 .s3 }}
{{- $podSecurityContext := default $.Values.podSecurityContext .podSecurityContext }}
{{- $containerSecurityContext := default $.Values.containerSecurityContext .containerSecurityContext }}
{{- $diagnosticMode := default $.Values.diagnosticMode .diagnosticMode }}
{{- $persistence := default $.Values.persistence .persistence }}
{{- $serviceAccount := default $.Values.serviceAccount .serviceAccount }}

{{- $resources := default $.Values.resources .resources }}
{{- $resourcesPreset := default $.Values.resourcesPreset .resourcesPreset }}
apiVersion: {{ include "common.capabilities.deployment.apiVersion" $ }}
kind: Deployment
metadata:
  name: {{ include "common.names.fullname" $ }}-{{ $type }}-{{ $name }}
  namespace: {{ include "common.names.namespace" $ | quote }}
  labels: 
    {{- include "common.labels.standard" ( dict "customLabels" $commonLabels "context" $ ) | nindent 4 }}
    app.kubernetes.io/component: {{ $component }}
  {{- with $commonAnnotations }}
  annotations: {{- include "common.tplvalues.render" ( dict "value" . "context" $ ) | nindent 4 }}
  {{- end }}
spec:
  {{- if not $autoscaling.enabled }}
  replicas: {{ $replicaCount }}
  {{- end }}
  revisionHistoryLimit: {{ $revisionHistoryLimit }}
  {{- if $updateStrategy }}
  strategy: {{- toYaml $updateStrategy | nindent 4 }}
  {{- end }}
  {{- $mergedPodLabels := include "common.tplvalues.merge" ( dict "values" ( list $podLabels $commonLabels ) "context" $ ) }}
  selector:
    matchLabels:
      {{- include "common.labels.matchLabels" ( dict "customLabels" $mergedPodLabels "context" $ ) | nindent 6 }}
      app.kubernetes.io/component: {{ $component }}
  template:
    metadata:
      labels:
        {{- include "common.labels.standard" ( dict "customLabels" $mergedPodLabels "context" $ ) | nindent 8 }}
        app.kubernetes.io/component: {{ $component }}
      annotations:
        {{- with $podAnnotations }}
        {{- include "common.tplvalues.render" ( dict "value" . "context" $) | nindent 8 }}
        {{- end }}
    spec:
      {{- include "kimi.imagePullSecrets" $ | nindent 6 }}
      shareProcessNamespace: {{ default $.Values.sidecarSingleProcessNamespace .sidecarSingleProcessNamespace }}
      serviceAccountName: {{ template "kimi.serviceAccountName" (dict "value" $serviceAccount "type" $type "name" $name "context" $ ) }}
      automountServiceAccountToken: {{ default $.Values.automountServiceAccountToken .automountServiceAccountToken }}
      {{- with default $.Values.hostAliases .hostAliases }}
      hostAliases: {{- include "common.tplvalues.render" (dict "value" . "context" $) | nindent 8 }}
      {{- end }}
      {{- with default $.Values.affinity .affinity }}
      affinity: {{- include "common.tplvalues.render" (dict "value" . "context" $) | nindent 8 }}
      {{- else }}
      affinity:
        podAffinity: {{- include "common.affinities.pods" (dict "type" $podAffinityPreset "component" $component "customLabels" $podLabels "context" $) | nindent 10 }}
        podAntiAffinity: {{- include "common.affinities.pods" (dict "type" $podAntiAffinityPreset "component" $component "customLabels" $podLabels "context" $) | nindent 10 }}
        nodeAffinity: {{- include "common.affinities.nodes" (dict "type" $nodeAffinityPreset.type "component" $component "key" $nodeAffinityPreset.key "values" $nodeAffinityPreset.values) | nindent 10 }}
      {{- end }}
      hostNetwork: {{ default $.Values.hostNetwork .hostNetwork }}
      {{- with (default $.Values.dnsConfig .dnsConfig) }}
      dnsPolicy: {{ . }}
      {{- end }}
      {{- with (default $.Values.dnsConfig .dnsConfig) }}
      dnsConfig: {{- include "common.tplvalues.render" (dict "value" . "context" $) | nindent 8 }}
      {{- end }}
      hostIPC: {{ default $.Values.hostIPC .hostIPC }}
      {{- with (default $.Values.priorityClassName .priorityClassName) }}
      priorityClassName: {{ . | quote }}
      {{- end }}
      {{- with (default $.Values.nodeSelector .nodeSelector) }}
      nodeSelector: {{- include "common.tplvalues.render" (dict "value" . "context" $) | nindent 8 }}
      {{- end }}
      {{- with (default $.Values.tolerations .tolerations) }}
      tolerations: {{- include "common.tplvalues.render" (dict "value" . "context" $) | nindent 8 }}
      {{- end }}
      {{- with (default $.Values.schedulerName .schedulerName) }}
      schedulerName: {{ . | quote }}
      {{- end }}
      {{- with (default $.Values.topologySpreadConstraints .topologySpreadConstraints) }}
      topologySpreadConstraints: {{- include "common.tplvalues.render" (dict "value" . "context" $) | nindent 8 }}
      {{- end }}
      {{- if $podSecurityContext.enabled }}
      securityContext: {{- include "common.compatibility.renderSecurityContext" (dict "secContext" $podSecurityContext "context" $) | nindent 8 }}
      {{- end }}
      {{- with (default $.Values.terminationGracePeriodSeconds .terminationGracePeriodSeconds) }}
      terminationGracePeriodSeconds: {{ . }}
      {{- end }}
      {{- with (default $.Values.initContainers .initContainers) }}
      initContainers:
      {{- include "common.tplvalues.render" (dict "value" . "context" $) | nindent 8 }}
      {{- end }}
      containers:
        - name: {{ $type }}
          image: {{ include "kimi.image" (dict "value" .image "context" $) }}
          imagePullPolicy: {{ .image.pullPolicy | quote }}
          {{- if $containerSecurityContext.enabled }}
          securityContext: {{- include "common.compatibility.renderSecurityContext" (dict "secContext" $containerSecurityContext "context" $) | nindent 12 }}
          {{- end }}
          {{- if $diagnosticMode.enabled }}
          command: {{- include "common.tplvalues.render" (dict "value" $diagnosticMode.command "context" $) | nindent 12 }}
          {{- else if .command }}
          command: {{- include "common.tplvalues.render" (dict "value" .command "context" $) | nindent 12 }}
          {{- end }}
          {{- if $diagnosticMode.enabled }}
          args: {{- include "common.tplvalues.render" (dict "value" $diagnosticMode.args "context" $) | nindent 12 }}
          {{- else if .args }}
          args: {{- include "common.tplvalues.render" (dict "value" .args "context" $) | nindent 12 }}
          {{- end }}
          {{- with default $.Values.lifecycleHooks .lifecycleHooks }}
          lifecycle: {{- include "common.tplvalues.render" (dict "value" . "context" $) | nindent 12 }}
          {{- end }}
          env:
            - name: AMQP_HOST
              value: {{ $amqp.host | quote }}
            - name: AMQP_PROTOCOL
              value: {{ $amqp.protocol | quote }}
            - name: AMQP_PORT
              value: {{ $amqp.port | quote }}
            - name: AMQP_USER
              value: {{ $amqp.username | quote }}
            - name: AMQP_PASS
              value: {{ $amqp.password | quote }}
            - name: STORAGE_PATH
              value: /tmp/station-data
            {{- if eq $type "worker" }}
            - name: EXCHANGE
              value: {{ .amqpExchange| quote }}
            - name: MINIO_HOST
              value: {{ $s3.host | quote }}
            - name: MINIO_PORT
              value: {{ $s3.port | quote }}
            - name: MINIO_ACCESS_KEY
              value: {{ $s3.accessKey | quote }}
            - name: MINIO_SECRET_KEY
              value: {{ $s3.secretKey | quote }}
            - name: MINIO_USE_SSL
              value: {{ $s3.useSSL | quote }}
            {{- end }}
            {{- if eq $type "station" }}
            - name: STATION_CONFIGURATION
              value: |
                {{ .configuration | toJson }}
            {{- end }}
            {{- with default $.Values.extraEnvVars .extraEnvVars }}
            {{- include "common.tplvalues.render" (dict "value" . "context" $) | nindent 12 }}
            {{- end }}
          envFrom:
            {{- with default $.Values.extraEnvVarsCM .extraEnvVarsCM }}
            - configMapRef:
                name: {{ include "common.tplvalues.render" (dict "value" . "context" $) }}
            {{- end }}
            {{- with default $.Values.extraEnvVarsSecret .extraEnvVarsSecret }}
            - secretRef:
                name: {{ include "common.tplvalues.render" (dict "value" . "context" $) }}
            {{- end }}
          {{- if $resources }}
          resources: {{- toYaml $resources | nindent 12 }}
          {{- else if ne $resourcesPreset "none" }}
          resources: {{- include "common.resources.preset" (dict "type" $resourcesPreset) | nindent 12 }}
          {{- end }}
          volumeMounts:
            - name: empty-dir
              mountPath: /tmp
              subPath: tmp-dir
            - mountPath: /data
              name: data
              subPath: data
            {{- with default $.Values.extraVolumeMounts .extraVolumeMounts }}
            {{- include "common.tplvalues.render" ( dict "value" . "context" $) | nindent 12 }}
            {{- end }}
        {{- with default $.Values.sidecars .sidecars }}
        {{- include "common.tplvalues.render" ( dict "value" . "context" $) | nindent 8 }}
        {{- end }}
      volumes:
        - name: empty-dir
          emptyDir: {}
        - name: data
          {{- if and $persistence.enabled (eq $type "station") }}
          persistentVolumeClaim:
            {{- if $persistence.existingClaim }}
            claimName: {{ $persistence.existingClaim }}
            {{- else }}
            claimName: {{ include "common.names.fullname" $ }}-{{ $type }}-{{ $name }}
            {{- end }}
          {{- else }}
          emptyDir: {}
          {{- end }}
        {{- with default $.Values.extraVolumes .extraVolumes }}
        {{- include "common.tplvalues.render" ( dict "value" . "context" $) | nindent 8 }}
        {{- end }}
---
{{- end }}
{{- end }}
