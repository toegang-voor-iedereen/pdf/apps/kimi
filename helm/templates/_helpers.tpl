{{/*
SPDX-License-Identifier: EUPL-1.2
*/}}

{{/* vim: set filetype=mustache: */}}
{{/*
Return the proper kimi image name
*/}}
{{- define "kimi.image" -}}
{{ include "common.images.image" (dict "imageRoot" .value "global" .context.Values.global) }}
{{- end -}}

{{/*
Return the proper Docker Image Registry Secret Names
*/}}
{{- define "kimi.imagePullSecrets" -}}
{{- include "common.images.pullSecrets" (dict "imageRoot" .Values.image "global" .Values.global) -}}
{{- end -}}

{{/*
Compile all warnings into a single message, and call fail.
*/}}
{{- define "kimi.validateValues" -}}
{{- $messages := list -}}
{{- $messages := append $messages (include "kimi.validateValues.extraVolumes" .) -}}
{{- $messages := without $messages "" -}}
{{- $message := join "\n" $messages -}}

{{- if $message -}}
{{-   printf "\nVALUES VALIDATION:\n%s" $message | fail -}}
{{- end -}}
{{- end -}}

{{/* Validate values of kimi - Incorrect extra volume settings */}}
{{- define "kimi.validateValues.extraVolumes" -}}
{{- if and (.Values.extraVolumes) (not .Values.extraVolumeMounts) }}
kimi: missing-extra-volume-mounts
    You specified extra volumes but not mount points for them. Please set
    the extraVolumeMounts value 
{{- end }}
{{- end -}}

{{/*
 Create the name of the service account to use
 */}}
{{- define "kimi.serviceAccountName" -}}
{{- if .value.create -}}
    {{ default (include "common.names.fullname" .context) .value.name }}-{{ .type }}--{{ .name }}
{{- else -}}
    {{ default "default" .value.name }}
{{- end -}}
{{- end -}}
