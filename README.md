# Kimi
This repository configures the setup of KIMI (the Knowledge Inducing Machinelearning Initiatve), an application from Logius that processes PDF documents to extract semantic meaning from its texts and images, and outputs (for now) an accessible HTML document.

For the outlay of working parts of this application, see the PDF repository's README:

https://gitlab.com/toegang-voor-iedereen/pdf

## Installation

See [gitlab.com/toegang-voor-iedereen/app/nldoc](https://gitlab.com/toegang-voor-iedereen/app/nldoc).
