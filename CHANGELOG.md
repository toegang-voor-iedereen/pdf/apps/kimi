# Changelog

## [3.0.9](https://gitlab.com/toegang-voor-iedereen/pdf/apps/kimi/compare/3.0.8...3.0.9) (2025-03-13)


### Bug Fixes

* use page-regions-yolo v1.17.8 ([3b33ceb](https://gitlab.com/toegang-voor-iedereen/pdf/apps/kimi/commit/3b33ceba5fce753cb62cea6a0273055e4a170659))

## [3.0.8](https://gitlab.com/toegang-voor-iedereen/pdf/apps/kimi/compare/3.0.7...3.0.8) (2025-03-13)


### Bug Fixes

* use alpha release of Yolo worker ([bcdd7f5](https://gitlab.com/toegang-voor-iedereen/pdf/apps/kimi/commit/bcdd7f5335c85d5a957bff185f8e584dca5098c9))

## [3.0.7](https://gitlab.com/toegang-voor-iedereen/pdf/apps/kimi/compare/3.0.6...3.0.7) (2025-03-13)


### Bug Fixes

* **deps:** update registry.gitlab.com/toegang-voor-iedereen/pdf/workers/document-metadata docker tag to v1.14.6 ([daeedd1](https://gitlab.com/toegang-voor-iedereen/pdf/apps/kimi/commit/daeedd1c541f607980629347b29e9be8ef87d4df))
* **deps:** update registry.gitlab.com/toegang-voor-iedereen/pdf/workers/document-mimetype-from-header docker tag to v1.20.3 ([8cf0976](https://gitlab.com/toegang-voor-iedereen/pdf/apps/kimi/commit/8cf09769013efba3d9bcc7795c9e0f1bee46e050))
* **deps:** update registry.gitlab.com/toegang-voor-iedereen/pdf/workers/document-pages docker tag to v1.13.3 ([e9efaec](https://gitlab.com/toegang-voor-iedereen/pdf/apps/kimi/commit/e9efaece363439c0065e2e4584db6781fdc0b9a3))
* **deps:** update registry.gitlab.com/toegang-voor-iedereen/pdf/workers/page-content docker tag to v1.18.3 ([4619776](https://gitlab.com/toegang-voor-iedereen/pdf/apps/kimi/commit/4619776c0d3e21519c0c7c90c1300778c7c89933))
* **deps:** update registry.gitlab.com/toegang-voor-iedereen/pdf/workers/page-content-images docker tag to v2.4.7 ([5532399](https://gitlab.com/toegang-voor-iedereen/pdf/apps/kimi/commit/5532399d5ed03be7b1c8ecd93a1929d284c33f15))
* **deps:** update registry.gitlab.com/toegang-voor-iedereen/pdf/workers/page-content-tables docker tag to v1.3.8 ([be9d677](https://gitlab.com/toegang-voor-iedereen/pdf/apps/kimi/commit/be9d6775dba0e4f3d8bcd2cb0b0820f82f3602be))
* **deps:** update registry.gitlab.com/toegang-voor-iedereen/pdf/workers/page-interpreted-content-ocr docker tag to v1.14.5 ([099fd8c](https://gitlab.com/toegang-voor-iedereen/pdf/apps/kimi/commit/099fd8c3709f5892d08a9cd97d370150fff5ec6b))
* **deps:** update registry.gitlab.com/toegang-voor-iedereen/pdf/workers/page-regions-image-processing docker tag to v1.14.8 ([b569463](https://gitlab.com/toegang-voor-iedereen/pdf/apps/kimi/commit/b56946388aebe8739da26769ef48bfde98ed5c27))
* **deps:** update registry.gitlab.com/toegang-voor-iedereen/pdf/workers/page-regions-yolo docker tag to v1.17.7 ([ce632ce](https://gitlab.com/toegang-voor-iedereen/pdf/apps/kimi/commit/ce632ce771524e73783912e8d4a3d38ac8613169))

## [3.0.6](https://gitlab.com/toegang-voor-iedereen/pdf/apps/kimi/compare/3.0.5...3.0.6) (2025-03-13)


### Bug Fixes

* **deps:** update registry.gitlab.com/toegang-voor-iedereen/pdf/workers/document-mimetype-from-header docker tag to v1.20.2 ([276df8b](https://gitlab.com/toegang-voor-iedereen/pdf/apps/kimi/commit/276df8bb35cb595ef9fb28467e8fd7b3cf7d35f8))

## [3.0.5](https://gitlab.com/toegang-voor-iedereen/pdf/apps/kimi/compare/3.0.4...3.0.5) (2025-03-13)


### Bug Fixes

* **deps:** update registry.gitlab.com/toegang-voor-iedereen/pdf/workers/document-metadata docker tag to v1.14.5 ([9c86d0c](https://gitlab.com/toegang-voor-iedereen/pdf/apps/kimi/commit/9c86d0cd3d565de0aa8009307907966c6f2ad7bb))
* **deps:** update registry.gitlab.com/toegang-voor-iedereen/pdf/workers/document-pages docker tag to v1.13.2 ([e2f3ba2](https://gitlab.com/toegang-voor-iedereen/pdf/apps/kimi/commit/e2f3ba21c22c188121742be85f40c6398a6507ba))
* **deps:** update registry.gitlab.com/toegang-voor-iedereen/pdf/workers/page-content docker tag to v1.18.2 ([e4a3e91](https://gitlab.com/toegang-voor-iedereen/pdf/apps/kimi/commit/e4a3e91113583926b2b0ba53b8e2f0ab85665b53))
* **deps:** update registry.gitlab.com/toegang-voor-iedereen/pdf/workers/page-content-images docker tag to v2.4.6 ([189829d](https://gitlab.com/toegang-voor-iedereen/pdf/apps/kimi/commit/189829d82012e5356024a7ce449d9a6fccc76578))
* **deps:** update registry.gitlab.com/toegang-voor-iedereen/pdf/workers/page-interpreted-content-ocr docker tag to v1.14.4 ([8222bea](https://gitlab.com/toegang-voor-iedereen/pdf/apps/kimi/commit/8222bea2f894b2d20c41590827e1b573195f473b))
* **deps:** update registry.gitlab.com/toegang-voor-iedereen/pdf/workers/page-regions-image-processing docker tag to v1.14.7 ([375c3d5](https://gitlab.com/toegang-voor-iedereen/pdf/apps/kimi/commit/375c3d5453fdf9198fd9eac00e0b6534f03cbc1d))
* **deps:** update registry.gitlab.com/toegang-voor-iedereen/pdf/workers/page-regions-yolo docker tag to v1.17.6 ([548c67e](https://gitlab.com/toegang-voor-iedereen/pdf/apps/kimi/commit/548c67e2f56ada1ddeded8c34c45f9ad19fa1b2e))

## [3.0.4](https://gitlab.com/toegang-voor-iedereen/pdf/apps/kimi/compare/3.0.3...3.0.4) (2025-03-13)


### Bug Fixes

* **deps:** update registry.gitlab.com/toegang-voor-iedereen/pdf/workers/document-output-nldocspec docker tag to v3.0.6 ([b4c6f86](https://gitlab.com/toegang-voor-iedereen/pdf/apps/kimi/commit/b4c6f86e9193549fa2903a9bae0721a73bdf205b))

## [3.0.3](https://gitlab.com/toegang-voor-iedereen/pdf/apps/kimi/compare/3.0.2...3.0.3) (2025-03-06)


### Bug Fixes

* **deps:** update registry.gitlab.com/toegang-voor-iedereen/pdf/workers/document-metadata docker tag to v1.14.4 ([baae969](https://gitlab.com/toegang-voor-iedereen/pdf/apps/kimi/commit/baae9699aa8dffe46ff937e3912a21ae7254e631))
* **deps:** update registry.gitlab.com/toegang-voor-iedereen/pdf/workers/page-content-images docker tag to v2.4.5 ([22094a8](https://gitlab.com/toegang-voor-iedereen/pdf/apps/kimi/commit/22094a8da8a57191940359fc343e6b8643737e7a))
* **deps:** update registry.gitlab.com/toegang-voor-iedereen/pdf/workers/page-interpreted-content-ocr docker tag to v1.14.3 ([5f5dc67](https://gitlab.com/toegang-voor-iedereen/pdf/apps/kimi/commit/5f5dc6745cf0db1015580b6c9213d63bfb75dd4e))
* **deps:** update registry.gitlab.com/toegang-voor-iedereen/pdf/workers/page-regions-image-processing docker tag to v1.14.5 ([1f374ef](https://gitlab.com/toegang-voor-iedereen/pdf/apps/kimi/commit/1f374ef43946d1d9e2bfc1a34852c8d9c2f627fb))
* **deps:** update registry.gitlab.com/toegang-voor-iedereen/pdf/workers/page-regions-yolo docker tag to v1.17.3 ([4add179](https://gitlab.com/toegang-voor-iedereen/pdf/apps/kimi/commit/4add179099fce721fa429e40319724be1c5a120c))

## [3.0.2](https://gitlab.com/toegang-voor-iedereen/pdf/apps/kimi/compare/3.0.1...3.0.2) (2025-03-06)


### Bug Fixes

* **deps:** update registry.gitlab.com/toegang-voor-iedereen/pdf/stations docker tag to v5.10.1 ([ad3ee0b](https://gitlab.com/toegang-voor-iedereen/pdf/apps/kimi/commit/ad3ee0bb0542d7d4b0ca51e6a6c679ab98eaaea1))
* **deps:** update registry.gitlab.com/toegang-voor-iedereen/pdf/workers/document-metadata docker tag to v1.14.3 ([2ac6c1a](https://gitlab.com/toegang-voor-iedereen/pdf/apps/kimi/commit/2ac6c1a367e44757cd6d92dfc1d48650d3b573ff))
* **deps:** update registry.gitlab.com/toegang-voor-iedereen/pdf/workers/document-output-nldocspec docker tag to v3.0.5 ([93c4a96](https://gitlab.com/toegang-voor-iedereen/pdf/apps/kimi/commit/93c4a96d47fa6bf7587d530b5381116b3e51e4f5))
* **deps:** update registry.gitlab.com/toegang-voor-iedereen/pdf/workers/page-content-images docker tag to v2.4.4 ([c78f6d9](https://gitlab.com/toegang-voor-iedereen/pdf/apps/kimi/commit/c78f6d9db7624f67836599b4b1d46512d96dcef2))
* **deps:** update registry.gitlab.com/toegang-voor-iedereen/pdf/workers/page-regions-image-processing docker tag to v1.14.3 ([20f5ce6](https://gitlab.com/toegang-voor-iedereen/pdf/apps/kimi/commit/20f5ce60ce49d5850d9416146bc03e2fc96b706a))

## [3.0.1](https://gitlab.com/toegang-voor-iedereen/pdf/apps/kimi/compare/3.0.0...3.0.1) (2025-03-05)


### Bug Fixes

* **deps:** update registry.gitlab.com/toegang-voor-iedereen/pdf/workers/document-metadata docker tag to v1.14.2 ([bdddc8a](https://gitlab.com/toegang-voor-iedereen/pdf/apps/kimi/commit/bdddc8a655ce29dbbea23428af3f05f12975334a))
* **deps:** update registry.gitlab.com/toegang-voor-iedereen/pdf/workers/page-interpreted-content-ocr docker tag to v1.14.2 ([7c1ea5c](https://gitlab.com/toegang-voor-iedereen/pdf/apps/kimi/commit/7c1ea5c14ad644a454950ec70fe0767365872ca4))

# [3.0.0](https://gitlab.com/toegang-voor-iedereen/pdf/apps/kimi/compare/2.59.11...3.0.0) (2025-03-04)


* feat!(NLD-564): Clean up chart ([6f063d4](https://gitlab.com/toegang-voor-iedereen/pdf/apps/kimi/commit/6f063d4e0092537e54f063219007a9f99eecc77f))


### Bug Fixes

* **deps:** Update dependency lock ([8af0800](https://gitlab.com/toegang-voor-iedereen/pdf/apps/kimi/commit/8af0800f49ff52e71978069a2b4a5f88736c3159))


### BREAKING CHANGES

* renames externalAmqp to amqp and externalS3 to s3

## [2.59.11](https://gitlab.com/toegang-voor-iedereen/pdf/apps/kimi/compare/2.59.10...2.59.11) (2025-02-13)


### Bug Fixes

* **deps:** update registry.gitlab.com/toegang-voor-iedereen/pdf/stations docker tag to v5.9.0 ([c743aea](https://gitlab.com/toegang-voor-iedereen/pdf/apps/kimi/commit/c743aea0463dc44e4f98dc205c947dc38d784fd0))

## [2.59.10](https://gitlab.com/toegang-voor-iedereen/pdf/apps/kimi/compare/2.59.9...2.59.10) (2025-02-11)


### Bug Fixes

* **deps:** update registry.gitlab.com/toegang-voor-iedereen/pdf/stations docker tag to v5.8.5 ([4e1486a](https://gitlab.com/toegang-voor-iedereen/pdf/apps/kimi/commit/4e1486ab1826ed5c82412459c61d7bd16a64f19c))
* **deps:** update registry.gitlab.com/toegang-voor-iedereen/pdf/workers/document-metadata docker tag to v1.14.0 ([d76702c](https://gitlab.com/toegang-voor-iedereen/pdf/apps/kimi/commit/d76702cc05a35085fe4163026d085c18faf8c5c3))
* **deps:** update registry.gitlab.com/toegang-voor-iedereen/pdf/workers/document-output-nldocspec docker tag to v3.0.3 ([70358a9](https://gitlab.com/toegang-voor-iedereen/pdf/apps/kimi/commit/70358a9edf473a8ce655eb47ba69cb0e23e77fdc))
* **deps:** update registry.gitlab.com/toegang-voor-iedereen/pdf/workers/page-regions-yolo docker tag to v1.17.2 ([d3062a5](https://gitlab.com/toegang-voor-iedereen/pdf/apps/kimi/commit/d3062a5bdba7177c040215ba0530c9217c8c90c3))

## [2.59.9](https://gitlab.com/toegang-voor-iedereen/pdf/apps/kimi/compare/2.59.8...2.59.9) (2025-01-20)


### Bug Fixes

* **deps:** update registry.gitlab.com/toegang-voor-iedereen/pdf/workers/page-interpreted-content-ocr docker tag to v1.14.1 ([c8aa7b1](https://gitlab.com/toegang-voor-iedereen/pdf/apps/kimi/commit/c8aa7b16fc4120f5ffc22b3d62954516539eb40d))
* **deps:** update registry.gitlab.com/toegang-voor-iedereen/pdf/workers/page-regions-image-processing docker tag to v1.14.1 ([fec3c9e](https://gitlab.com/toegang-voor-iedereen/pdf/apps/kimi/commit/fec3c9e6a9546de288bb36179f55df07b3421f37))

## [2.59.8](https://gitlab.com/toegang-voor-iedereen/pdf/apps/kimi/compare/2.59.7...2.59.8) (2025-01-20)


### Bug Fixes

* **deps:** update registry.gitlab.com/toegang-voor-iedereen/pdf/workers/page-content docker tag to v1.18.1 ([0340142](https://gitlab.com/toegang-voor-iedereen/pdf/apps/kimi/commit/034014299d1ba9bbcee92a067538d989acf1e47b))

## [2.59.7](https://gitlab.com/toegang-voor-iedereen/pdf/apps/kimi/compare/2.59.6...2.59.7) (2025-01-20)


### Bug Fixes

* **deps:** update registry.gitlab.com/toegang-voor-iedereen/pdf/workers/document-mimetype-from-header docker tag to v1.20.1 ([2067407](https://gitlab.com/toegang-voor-iedereen/pdf/apps/kimi/commit/2067407ede697b76f5b75efc3acb340e6f051f75))

## [2.59.6](https://gitlab.com/toegang-voor-iedereen/pdf/apps/kimi/compare/2.59.5...2.59.6) (2025-01-20)


### Bug Fixes

* **deps:** update registry.gitlab.com/toegang-voor-iedereen/pdf/workers/document-metadata docker tag to v1.13.1 ([02c1f25](https://gitlab.com/toegang-voor-iedereen/pdf/apps/kimi/commit/02c1f25d7beaa6b37f0157e289e25a322ff9ed89))
* **deps:** update registry.gitlab.com/toegang-voor-iedereen/pdf/workers/document-pages docker tag to v1.13.1 ([811a808](https://gitlab.com/toegang-voor-iedereen/pdf/apps/kimi/commit/811a8085c7a939109daaf9ebe632de6dd1a40163))
* **deps:** update registry.gitlab.com/toegang-voor-iedereen/pdf/workers/page-content-images docker tag to v2.4.1 ([c30ca1a](https://gitlab.com/toegang-voor-iedereen/pdf/apps/kimi/commit/c30ca1aff79d528f797de71877d4a9ce54cb4431))

## [2.59.5](https://gitlab.com/toegang-voor-iedereen/pdf/apps/kimi/compare/2.59.4...2.59.5) (2025-01-20)


### Bug Fixes

* **deps:** update registry.gitlab.com/toegang-voor-iedereen/pdf/workers/page-content-tables docker tag to v1.3.2 ([ecb7d90](https://gitlab.com/toegang-voor-iedereen/pdf/apps/kimi/commit/ecb7d9003277aba5be0cfafc7a82c9a01fa49eb7))

## [2.59.4](https://gitlab.com/toegang-voor-iedereen/pdf/apps/kimi/compare/2.59.3...2.59.4) (2025-01-20)


### Bug Fixes

* **deps:** update registry.gitlab.com/toegang-voor-iedereen/pdf/stations docker tag to v5.8.4 ([40e3e85](https://gitlab.com/toegang-voor-iedereen/pdf/apps/kimi/commit/40e3e85e096fb70cba910d840512a1bffe0d1c85))

## [2.59.3](https://gitlab.com/toegang-voor-iedereen/pdf/apps/kimi/compare/2.59.2...2.59.3) (2025-01-16)


### Bug Fixes

* **deps:** update registry.gitlab.com/toegang-voor-iedereen/pdf/workers/page-content-tables docker tag to v1.3.1 ([3e53f3e](https://gitlab.com/toegang-voor-iedereen/pdf/apps/kimi/commit/3e53f3ecc0dea4140c9aafa93c1107d56b1b5496))
* **deps:** update registry.gitlab.com/toegang-voor-iedereen/pdf/workers/page-regions-yolo docker tag to v1.17.1 ([1b0ff96](https://gitlab.com/toegang-voor-iedereen/pdf/apps/kimi/commit/1b0ff96e9fcb507c9c83ea864d2f3952bc2243dc))

## [2.59.2](https://gitlab.com/toegang-voor-iedereen/pdf/apps/kimi/compare/2.59.1...2.59.2) (2025-01-15)


### Bug Fixes

* **deps:** update registry.gitlab.com/toegang-voor-iedereen/pdf/workers/page-content-images docker tag to v2.4.0 ([0bac083](https://gitlab.com/toegang-voor-iedereen/pdf/apps/kimi/commit/0bac08371155411a17776d47b30f89c4807267e4))

## [2.59.1](https://gitlab.com/toegang-voor-iedereen/pdf/apps/kimi/compare/2.59.0...2.59.1) (2025-01-15)


### Bug Fixes

* **deps:** update registry.gitlab.com/toegang-voor-iedereen/pdf/stations docker tag to v5.8.3 ([ca28ff5](https://gitlab.com/toegang-voor-iedereen/pdf/apps/kimi/commit/ca28ff53a7f5218c423a920a4fb5aaaaac43e162))
* **deps:** update registry.gitlab.com/toegang-voor-iedereen/pdf/workers/page-content-tables docker tag to v1.2.1 ([60bb841](https://gitlab.com/toegang-voor-iedereen/pdf/apps/kimi/commit/60bb8410835ff1f4d3c4b9ac5240799a8ee5ddb7))

# [2.59.0](https://gitlab.com/toegang-voor-iedereen/pdf/apps/kimi/compare/2.58.0...2.59.0) (2025-01-15)


### Features

* stations v5.8.2 ([f47a3a2](https://gitlab.com/toegang-voor-iedereen/pdf/apps/kimi/commit/f47a3a2abc87c73a22d385a705dfcfb85f4c42b4))

# [2.58.0](https://gitlab.com/toegang-voor-iedereen/pdf/apps/kimi/compare/2.57.0...2.58.0) (2025-01-15)


### Features

* station v5.9.0 ([a6857c2](https://gitlab.com/toegang-voor-iedereen/pdf/apps/kimi/commit/a6857c2940e341b8cecb8b81c14cd6f78e3568d9))
* stations v5.8.1 ([feccd3c](https://gitlab.com/toegang-voor-iedereen/pdf/apps/kimi/commit/feccd3cc549d9adea4365ecc703001fe1dfc3d6d))

# [2.57.0](https://gitlab.com/toegang-voor-iedereen/pdf/apps/kimi/compare/2.56.0...2.57.0) (2025-01-15)


### Features

* stations v5.8.0 ([653ce41](https://gitlab.com/toegang-voor-iedereen/pdf/apps/kimi/commit/653ce4105e0e9e0a5e60769265f51ab61aec8fa2))

# [2.56.0](https://gitlab.com/toegang-voor-iedereen/pdf/apps/kimi/compare/2.55.0...2.56.0) (2025-01-14)


### Features

* stations v5.7.0 with improved attribute reading performance ([17d594b](https://gitlab.com/toegang-voor-iedereen/pdf/apps/kimi/commit/17d594bc18a3e6f5355d1478276858f624acaff5))

# [2.55.0](https://gitlab.com/toegang-voor-iedereen/pdf/apps/kimi/compare/2.54.8...2.55.0) (2025-01-14)


### Features

* station v5.6.0 ([71ae269](https://gitlab.com/toegang-voor-iedereen/pdf/apps/kimi/commit/71ae269f04020e05c2753d969363483175665bce))

## [2.54.8](https://gitlab.com/toegang-voor-iedereen/pdf/apps/kimi/compare/2.54.7...2.54.8) (2025-01-14)


### Bug Fixes

* **deps:** update registry.gitlab.com/toegang-voor-iedereen/pdf/stations docker tag to v5.4.0 ([045e481](https://gitlab.com/toegang-voor-iedereen/pdf/apps/kimi/commit/045e4819b320bc8f20950a20a27266319ddd7575))

## [2.54.7](https://gitlab.com/toegang-voor-iedereen/pdf/apps/kimi/compare/2.54.6...2.54.7) (2025-01-14)


### Bug Fixes

* **deps:** update registry.gitlab.com/toegang-voor-iedereen/pdf/stations docker tag to v5.3.1 ([3640949](https://gitlab.com/toegang-voor-iedereen/pdf/apps/kimi/commit/3640949396f5645d399e4ca7ecfaeaf3e6e214a7))

## [2.54.6](https://gitlab.com/toegang-voor-iedereen/pdf/apps/kimi/compare/2.54.5...2.54.6) (2025-01-14)


### Bug Fixes

* **deps:** update registry.gitlab.com/toegang-voor-iedereen/pdf/stations docker tag to v5.3.0 ([6f49666](https://gitlab.com/toegang-voor-iedereen/pdf/apps/kimi/commit/6f49666dfea7f50f9e837b0c74adde49fb07249f))

## [2.54.5](https://gitlab.com/toegang-voor-iedereen/pdf/apps/kimi/compare/2.54.4...2.54.5) (2025-01-14)


### Bug Fixes

* **deps:** update registry.gitlab.com/toegang-voor-iedereen/pdf/stations docker tag to v5.2.2 ([ef3fc74](https://gitlab.com/toegang-voor-iedereen/pdf/apps/kimi/commit/ef3fc7499280bcb41557dc11df81b678164a89cd))

## [2.54.4](https://gitlab.com/toegang-voor-iedereen/pdf/apps/kimi/compare/2.54.3...2.54.4) (2025-01-14)


### Bug Fixes

* **deps:** update registry.gitlab.com/toegang-voor-iedereen/pdf/stations docker tag to v5.2.0 ([ef70123](https://gitlab.com/toegang-voor-iedereen/pdf/apps/kimi/commit/ef70123d6a71f29859ee2cd1697c37b61697aa2d))

## [2.54.3](https://gitlab.com/toegang-voor-iedereen/pdf/apps/kimi/compare/2.54.2...2.54.3) (2025-01-09)


### Bug Fixes

* **deps:** update registry.gitlab.com/toegang-voor-iedereen/pdf/stations docker tag to v5.1.1 ([3fd96ad](https://gitlab.com/toegang-voor-iedereen/pdf/apps/kimi/commit/3fd96ad6f96c667c3d8c9c3d39c66dc6f4758dcd))

## [2.54.2](https://gitlab.com/toegang-voor-iedereen/pdf/apps/kimi/compare/2.54.1...2.54.2) (2025-01-09)


### Bug Fixes

* **deps:** update registry.gitlab.com/toegang-voor-iedereen/pdf/stations docker tag to v5.1.0 ([01c75d6](https://gitlab.com/toegang-voor-iedereen/pdf/apps/kimi/commit/01c75d61cdb4cbb6fba00e2e72d838a372b83f28))

## [2.54.1](https://gitlab.com/toegang-voor-iedereen/pdf/apps/kimi/compare/2.54.0...2.54.1) (2025-01-06)


### Bug Fixes

* **deps:** update registry.gitlab.com/toegang-voor-iedereen/pdf/stations docker tag to v5 ([b3f3a82](https://gitlab.com/toegang-voor-iedereen/pdf/apps/kimi/commit/b3f3a82271760248936faadfd73de831a4c216e7))

# [2.54.0](https://gitlab.com/toegang-voor-iedereen/pdf/apps/kimi/compare/2.53.0...2.54.0) (2024-12-17)


### Features

* **deps:** all workers to newest version ([a8bc9bf](https://gitlab.com/toegang-voor-iedereen/pdf/apps/kimi/commit/a8bc9bffb7587f35fc7c597ff2eaf8014c8ceb18))

# [2.53.0](https://gitlab.com/toegang-voor-iedereen/pdf/apps/kimi/compare/2.52.4...2.53.0) (2024-12-11)


### Features

* **deps:** document-mimetype-from-header v1.18.0 ([df3f72b](https://gitlab.com/toegang-voor-iedereen/pdf/apps/kimi/commit/df3f72b3bcaa773edcaa5b82c91991a302ee3050))
* **deps:** document-output-nldocspec v2.0.0 ([b46e0d0](https://gitlab.com/toegang-voor-iedereen/pdf/apps/kimi/commit/b46e0d0645b6dbb37ff50753f7a52d4ae9c3d720))

## [2.52.4](https://gitlab.com/toegang-voor-iedereen/pdf/apps/kimi/compare/2.52.3...2.52.4) (2024-12-05)


### Bug Fixes

* **deps:** update registry.gitlab.com/toegang-voor-iedereen/pdf/workers/page-regions-yolo docker tag to v1.15.0 ([099e39b](https://gitlab.com/toegang-voor-iedereen/pdf/apps/kimi/commit/099e39b467082b5e258268e33341859bb8b16a66))

## [2.52.3](https://gitlab.com/toegang-voor-iedereen/pdf/apps/kimi/compare/2.52.2...2.52.3) (2024-12-05)


### Bug Fixes

* use page-regions-yolo ([cd758c9](https://gitlab.com/toegang-voor-iedereen/pdf/apps/kimi/commit/cd758c9d06d15e3d280a58f81254f0ed99fb7575))

## [2.52.2](https://gitlab.com/toegang-voor-iedereen/pdf/apps/kimi/compare/2.52.1...2.52.2) (2024-12-05)


### Bug Fixes

* **deps:** update registry.gitlab.com/toegang-voor-iedereen/pdf/workers/page-regions-image-processing docker tag to v1.13.2 ([5acad18](https://gitlab.com/toegang-voor-iedereen/pdf/apps/kimi/commit/5acad18f9c2a03a170de5050739dd35cb031b158))

## [2.52.1](https://gitlab.com/toegang-voor-iedereen/pdf/apps/kimi/compare/2.52.0...2.52.1) (2024-12-05)


### Bug Fixes

* worker dep name to base name ([dd8be69](https://gitlab.com/toegang-voor-iedereen/pdf/apps/kimi/commit/dd8be69fd360cf317feb465e36b755c91584026a))

# [2.52.0](https://gitlab.com/toegang-voor-iedereen/pdf/apps/kimi/compare/2.51.4...2.52.0) (2024-12-05)


### Bug Fixes

* artificial downgrading of worker to test renovate ([9dd019a](https://gitlab.com/toegang-voor-iedereen/pdf/apps/kimi/commit/9dd019ae417383ff08aa6c628bb9b705e935beb8))


### Features

* added worker and station dep name ([e8bb458](https://gitlab.com/toegang-voor-iedereen/pdf/apps/kimi/commit/e8bb458ab0118f1bc42b97eed61e2af50d435397))

## [2.51.4](https://gitlab.com/toegang-voor-iedereen/pdf/apps/kimi/compare/2.51.3...2.51.4) (2024-12-05)


### Bug Fixes

* repositoryUrl with datasource `docker` ([83095c8](https://gitlab.com/toegang-voor-iedereen/pdf/apps/kimi/commit/83095c87c473c965a7da119a83fb355cf1e643cf))

## [2.51.3](https://gitlab.com/toegang-voor-iedereen/pdf/apps/kimi/compare/2.51.2...2.51.3) (2024-12-05)


### Bug Fixes

* removed registryUrl from custom regex helm match ([66ebeb9](https://gitlab.com/toegang-voor-iedereen/pdf/apps/kimi/commit/66ebeb933f567b01ec38ac508ab3f50c0ac6cae2))

## [2.51.2](https://gitlab.com/toegang-voor-iedereen/pdf/apps/kimi/compare/2.51.1...2.51.2) (2024-12-05)


### Bug Fixes

* added registry to depName ([831db8a](https://gitlab.com/toegang-voor-iedereen/pdf/apps/kimi/commit/831db8a225610db92c64af6e2990ccb4e43ce368))

## [2.51.1](https://gitlab.com/toegang-voor-iedereen/pdf/apps/kimi/compare/2.51.0...2.51.1) (2024-12-05)


### Bug Fixes

* removed datasource and versioning static values and replaced with templates ([0b425bb](https://gitlab.com/toegang-voor-iedereen/pdf/apps/kimi/commit/0b425bb41f286daeb895abc365d0973892c62854))

# [2.51.0](https://gitlab.com/toegang-voor-iedereen/pdf/apps/kimi/compare/2.50.0...2.51.0) (2024-12-05)


### Features

* added custom managers for renovate to detect workers and stations ([2b81881](https://gitlab.com/toegang-voor-iedereen/pdf/apps/kimi/commit/2b81881db2cda17318833ac8a83781e2877f085f))

# [2.50.0](https://gitlab.com/toegang-voor-iedereen/pdf/apps/kimi/compare/2.49.0...2.50.0) (2024-12-03)


### Features

* page-regions-image-processing to v1.13.2 ([62234a1](https://gitlab.com/toegang-voor-iedereen/pdf/apps/kimi/commit/62234a103f5316cab85d714ba7c6227404900378))

# [2.49.0](https://gitlab.com/toegang-voor-iedereen/pdf/apps/kimi/compare/2.48.0...2.49.0) (2024-12-03)


### Features

* update page-regions-yolov8 to v1.14.1 ([e41b43b](https://gitlab.com/toegang-voor-iedereen/pdf/apps/kimi/commit/e41b43b6be293b898d55fb510b456f9ed8266b94))

# [2.48.0](https://gitlab.com/toegang-voor-iedereen/pdf/apps/kimi/compare/2.47.0...2.48.0) (2024-12-02)


### Features

* document-output-nldocspec v1.12.0 with improved list item prefix removal ([cc7476c](https://gitlab.com/toegang-voor-iedereen/pdf/apps/kimi/commit/cc7476ca9426cf5edd849397aea77019a4a99537))

# [2.47.0](https://gitlab.com/toegang-voor-iedereen/pdf/apps/kimi/compare/2.46.0...2.47.0) (2024-12-02)


### Features

* page-interpreted-content-ocr v1.13.1 which fixes the crash on empty pages ([9c3bb0e](https://gitlab.com/toegang-voor-iedereen/pdf/apps/kimi/commit/9c3bb0ec3aae2bc0b7fb736f368ba2827fc08717))

# [2.46.0](https://gitlab.com/toegang-voor-iedereen/pdf/apps/kimi/compare/2.45.0...2.46.0) (2024-12-02)


### Features

* document-output-nldocspec v1.11.3 with content encoding for json output ([a2dfe7b](https://gitlab.com/toegang-voor-iedereen/pdf/apps/kimi/commit/a2dfe7b34b915e06d7fb9a11741d95ac3ce75ab7))
* page-regions-image-processing v1.13.1 ([2a587e6](https://gitlab.com/toegang-voor-iedereen/pdf/apps/kimi/commit/2a587e61ae5c00f1b9183c70ca7c5ff5170e22b4))
* use stations v4.10.1 which switches back to file storage ([62de5da](https://gitlab.com/toegang-voor-iedereen/pdf/apps/kimi/commit/62de5da9f23de3749dfe0660c85e4f639ae6ea5b))

# [2.45.0](https://gitlab.com/toegang-voor-iedereen/pdf/apps/kimi/compare/2.44.0...2.45.0) (2024-12-02)


### Features

* page-content v1.17.0 which improves regio matching ([d53c8cb](https://gitlab.com/toegang-voor-iedereen/pdf/apps/kimi/commit/d53c8cb0f277575ea5114357e6147880381582fa))

# [2.44.0](https://gitlab.com/toegang-voor-iedereen/pdf/apps/kimi/compare/2.43.0...2.44.0) (2024-12-02)


### Features

* updated to station v4.10.0 that uses fixed MemoryStorage instead of FileStorage ([6896947](https://gitlab.com/toegang-voor-iedereen/pdf/apps/kimi/commit/689694727d62fbb06992880d10cfe97e9ae78d5f))

# [2.43.0](https://gitlab.com/toegang-voor-iedereen/pdf/apps/kimi/compare/2.42.1...2.43.0) (2024-12-02)


### Features

* v1.13.0 of page-regions-image-processing ([add9e47](https://gitlab.com/toegang-voor-iedereen/pdf/apps/kimi/commit/add9e4763461446a2fb9a3d861580c361ae47c0a))

## [2.42.1](https://gitlab.com/toegang-voor-iedereen/pdf/apps/kimi/compare/2.42.0...2.42.1) (2024-11-28)


### Bug Fixes

* v1.16.1 of page-content worker ([7fa9b99](https://gitlab.com/toegang-voor-iedereen/pdf/apps/kimi/commit/7fa9b991f30274678d5292fbfddabf4a86b436a4))

# [2.42.0](https://gitlab.com/toegang-voor-iedereen/pdf/apps/kimi/compare/2.41.0...2.42.0) (2024-11-28)


### Features

* stations v4.9.0 with quicker file locks and prefetch of 1 instead of 10 ([6981925](https://gitlab.com/toegang-voor-iedereen/pdf/apps/kimi/commit/6981925ad9a78f31725ac87816f420d442ac54cf))

# [2.41.0](https://gitlab.com/toegang-voor-iedereen/pdf/apps/kimi/compare/2.40.0...2.41.0) (2024-11-28)


### Features

* storage to non-pvc temp folder, revert station to v4.7.0 ([1170571](https://gitlab.com/toegang-voor-iedereen/pdf/apps/kimi/commit/117057185d7ee79c1be8180dfc874cd20ebea36a))

# [2.40.0](https://gitlab.com/toegang-voor-iedereen/pdf/apps/kimi/compare/2.39.0...2.40.0) (2024-11-28)


### Features

* stations v4.8.0 which uses memorystorage instead of file storage ([11a5cf6](https://gitlab.com/toegang-voor-iedereen/pdf/apps/kimi/commit/11a5cf6da2a2477f12283070b2bfcff26a8e4ae3))

# [2.39.0](https://gitlab.com/toegang-voor-iedereen/pdf/apps/kimi/compare/2.38.2...2.39.0) (2024-11-28)


### Features

* station to v4.7.0 with improved logging for job ids ([6883418](https://gitlab.com/toegang-voor-iedereen/pdf/apps/kimi/commit/688341895062e5c7daca8465097f74ec31e1b7c4))

## [2.38.2](https://gitlab.com/toegang-voor-iedereen/pdf/apps/kimi/compare/2.38.1...2.38.2) (2024-11-28)


### Bug Fixes

* page-regions-image-processing to v1.12.1 ([cf1414d](https://gitlab.com/toegang-voor-iedereen/pdf/apps/kimi/commit/cf1414de9b316f61f48ed9986561cb7cd7a98bf4))

## [2.38.1](https://gitlab.com/toegang-voor-iedereen/pdf/apps/kimi/compare/2.38.0...2.38.1) (2024-11-27)


### Bug Fixes

* station v4.6.2 ([f577f21](https://gitlab.com/toegang-voor-iedereen/pdf/apps/kimi/commit/f577f21217181d8f48284d7ccb625abf23d0a9b4))

# [2.38.0](https://gitlab.com/toegang-voor-iedereen/pdf/apps/kimi/compare/2.37.3...2.38.0) (2024-11-27)


### Features

* all stations to v4.6.1 ([f6649fd](https://gitlab.com/toegang-voor-iedereen/pdf/apps/kimi/commit/f6649fd65f5b49ac09ae649e0b1c4c439a9e4e88))

## [2.37.3](https://gitlab.com/toegang-voor-iedereen/pdf/apps/kimi/compare/2.37.2...2.37.3) (2024-11-25)


### Bug Fixes

* new version of page content tables ([84f43fd](https://gitlab.com/toegang-voor-iedereen/pdf/apps/kimi/commit/84f43fd0e0ba8228bee28d2fe60f6989c0ba72a8))

## [2.37.2](https://gitlab.com/toegang-voor-iedereen/pdf/apps/kimi/compare/2.37.1...2.37.2) (2024-11-21)


### Bug Fixes

* page-content-images to v2.2.1 ([a91c7f6](https://gitlab.com/toegang-voor-iedereen/pdf/apps/kimi/commit/a91c7f62989765560e564323368f33c8442d384d))

## [2.37.1](https://gitlab.com/toegang-voor-iedereen/pdf/apps/kimi/compare/2.37.0...2.37.1) (2024-11-21)


### Bug Fixes

* document-output-nldocspec v1.11.2 ([f575d50](https://gitlab.com/toegang-voor-iedereen/pdf/apps/kimi/commit/f575d50e49f0059202663e1a7f2e1476a85d3773))

# [2.37.0](https://gitlab.com/toegang-voor-iedereen/pdf/apps/kimi/compare/2.36.1...2.37.0) (2024-11-21)


### Features

* page-content-tables v1.1.1 ([3c4a68b](https://gitlab.com/toegang-voor-iedereen/pdf/apps/kimi/commit/3c4a68b9c4eb21319f3870209d1401bcd9df194a))

## [2.36.1](https://gitlab.com/toegang-voor-iedereen/pdf/apps/kimi/compare/2.36.0...2.36.1) (2024-11-21)


### Bug Fixes

* update page-content-tables to v1.1.0 ([2c46c57](https://gitlab.com/toegang-voor-iedereen/pdf/apps/kimi/commit/2c46c57c7e7601cdd8503d6aa0f4781014490b57))

# [2.36.0](https://gitlab.com/toegang-voor-iedereen/pdf/apps/kimi/compare/2.35.1...2.36.0) (2024-11-21)


### Features

* add page-content-tables worker ([6574832](https://gitlab.com/toegang-voor-iedereen/pdf/apps/kimi/commit/6574832c18a12f36a475d93a13728526784a9fd6))

## [2.35.1](https://gitlab.com/toegang-voor-iedereen/pdf/apps/kimi/compare/2.35.0...2.35.1) (2024-11-21)


### Bug Fixes

* removed required value "metadata" for output station ([9c06ffd](https://gitlab.com/toegang-voor-iedereen/pdf/apps/kimi/commit/9c06ffd895e9ecc7cbc90e266579ccaa0f22c09f))

# [2.35.0](https://gitlab.com/toegang-voor-iedereen/pdf/apps/kimi/compare/2.34.0...2.35.0) (2024-11-20)


### Features

* update stations to v4.6.0 which removes the handledMessage from logs ([c1eaa27](https://gitlab.com/toegang-voor-iedereen/pdf/apps/kimi/commit/c1eaa27d6fa3a5b4718ff5d0656b84acfd1bd198))

# [2.34.0](https://gitlab.com/toegang-voor-iedereen/pdf/apps/kimi/compare/2.33.0...2.34.0) (2024-11-19)


### Features

* latest worker versions ([55c56af](https://gitlab.com/toegang-voor-iedereen/pdf/apps/kimi/commit/55c56af8f0eda8d6424dd6f654f5c8fe38299bfd))

# [2.33.0](https://gitlab.com/toegang-voor-iedereen/pdf/apps/kimi/compare/2.32.0...2.33.0) (2024-11-18)


### Features

* document-output-nldocspec v1.10.0 which removes large logs ([f95ec4a](https://gitlab.com/toegang-voor-iedereen/pdf/apps/kimi/commit/f95ec4a4a6560093931e087ed29ee49be397208a))

# [2.32.0](https://gitlab.com/toegang-voor-iedereen/pdf/apps/kimi/compare/2.31.1...2.32.0) (2024-11-14)


### Features

* page-content v1.15.0 and document-output-nldocspec v1.9.0 ([9619c92](https://gitlab.com/toegang-voor-iedereen/pdf/apps/kimi/commit/9619c924b9a141c3bb5f6410fe543372ca1b3431))

## [2.31.1](https://gitlab.com/toegang-voor-iedereen/pdf/apps/kimi/compare/2.31.0...2.31.1) (2024-11-14)


### Bug Fixes

* removed env vars ([8fc63a8](https://gitlab.com/toegang-voor-iedereen/pdf/apps/kimi/commit/8fc63a887f902d586f46d466d4989b6d64738619))

# [2.31.0](https://gitlab.com/toegang-voor-iedereen/pdf/apps/kimi/compare/2.30.0...2.31.0) (2024-11-14)


### Features

* document-output-nldocspec 1.8.0 ([0d30a09](https://gitlab.com/toegang-voor-iedereen/pdf/apps/kimi/commit/0d30a09e88c30b6eb96ec2e78426cd2873734808))

# [2.30.0](https://gitlab.com/toegang-voor-iedereen/pdf/apps/kimi/compare/2.29.0...2.30.0) (2024-11-14)


### Features

* set env var for min confidence ([779c484](https://gitlab.com/toegang-voor-iedereen/pdf/apps/kimi/commit/779c4846b89da917e2dea5eca81c94ae24d03692))

# [2.29.0](https://gitlab.com/toegang-voor-iedereen/pdf/apps/kimi/compare/2.28.0...2.29.0) (2024-11-14)


### Features

* document-output-nldocspec v1.7.0 with minimum confidence 0.5 ([e2b9772](https://gitlab.com/toegang-voor-iedereen/pdf/apps/kimi/commit/e2b977205eead6d02dfd141d8503af0d2c591da1))

# [2.28.0](https://gitlab.com/toegang-voor-iedereen/pdf/apps/kimi/compare/2.27.0...2.28.0) (2024-11-14)


### Features

* worker page-content-images v2.1.0, that renders tables as images ([c9748eb](https://gitlab.com/toegang-voor-iedereen/pdf/apps/kimi/commit/c9748ebb70c21ae9e03e261eead1f940c9e4bcb0))

# [2.27.0](https://gitlab.com/toegang-voor-iedereen/pdf/apps/kimi/compare/2.26.0...2.27.0) (2024-11-14)


### Features

* document-output-nldocspec v1.6.1 ([568e119](https://gitlab.com/toegang-voor-iedereen/pdf/apps/kimi/commit/568e1195197337c26c0656c0e377e41d3b7f5dd0))

# [2.26.0](https://gitlab.com/toegang-voor-iedereen/pdf/apps/kimi/compare/2.25.0...2.26.0) (2024-11-14)


### Features

* update docspec worker ([06c8b08](https://gitlab.com/toegang-voor-iedereen/pdf/apps/kimi/commit/06c8b08af8bd9cc6346656163e1313accac59ee7))

# [2.25.0](https://gitlab.com/toegang-voor-iedereen/pdf/apps/kimi/compare/2.24.0...2.25.0) (2024-11-14)


### Features

* page-content v1.14.0 ([d7bea57](https://gitlab.com/toegang-voor-iedereen/pdf/apps/kimi/commit/d7bea573755c8b7c20dee2e92928383a46499d14))

# [2.24.0](https://gitlab.com/toegang-voor-iedereen/pdf/apps/kimi/compare/2.23.0...2.24.0) (2024-11-14)


### Features

* page-regions-image-processing v1.11.0 ([cc0cb7a](https://gitlab.com/toegang-voor-iedereen/pdf/apps/kimi/commit/cc0cb7abcaae85f3554bc1ab7a1f74e8e2aeef68))

# [2.23.0](https://gitlab.com/toegang-voor-iedereen/pdf/apps/kimi/compare/2.22.0...2.23.0) (2024-11-13)


### Features

* document-output-nldocspec v1.5.2 ([d8ad4c0](https://gitlab.com/toegang-voor-iedereen/pdf/apps/kimi/commit/d8ad4c054bd0ac9fe1b63c16c53006e6664e0ccc))

# [2.22.0](https://gitlab.com/toegang-voor-iedereen/pdf/apps/kimi/compare/2.21.1...2.22.0) (2024-11-13)


### Features

* page-interpreted-content-ocr v1.12.0 ([980a6df](https://gitlab.com/toegang-voor-iedereen/pdf/apps/kimi/commit/980a6df9641ff15a0cdf8d1c6c51ed5464a9582b))

## [2.21.1](https://gitlab.com/toegang-voor-iedereen/pdf/apps/kimi/compare/2.21.0...2.21.1) (2024-11-13)


### Bug Fixes

* restored image name ([4ff4ec6](https://gitlab.com/toegang-voor-iedereen/pdf/apps/kimi/commit/4ff4ec64f0390b0c6407777eaf3b2599ba9d21b7))

# [2.21.0](https://gitlab.com/toegang-voor-iedereen/pdf/apps/kimi/compare/2.20.4...2.21.0) (2024-11-13)


### Features

* updated all workers to final versions without nldocspec and newest kimiworkers ([782a4d6](https://gitlab.com/toegang-voor-iedereen/pdf/apps/kimi/commit/782a4d6db1c59b96bc86ce22d989218f5c997784))

## [2.20.4](https://gitlab.com/toegang-voor-iedereen/pdf/apps/kimi/compare/2.20.3...2.20.4) (2024-11-13)


### Bug Fixes

* output to nldoc spec v1.4.2 ([3be3015](https://gitlab.com/toegang-voor-iedereen/pdf/apps/kimi/commit/3be30151d4cf041608ec9de5bcda0f44267a8824))

## [2.20.3](https://gitlab.com/toegang-voor-iedereen/pdf/apps/kimi/compare/2.20.2...2.20.3) (2024-11-12)


### Bug Fixes

* selector ([62f7052](https://gitlab.com/toegang-voor-iedereen/pdf/apps/kimi/commit/62f705283b6c01bf30e1893df22967da48f2287f))

## [2.20.2](https://gitlab.com/toegang-voor-iedereen/pdf/apps/kimi/compare/2.20.1...2.20.2) (2024-11-12)


### Bug Fixes

* antiAffinity and labels fix component ([4f04f59](https://gitlab.com/toegang-voor-iedereen/pdf/apps/kimi/commit/4f04f5908214de508b88cd05aaed659a0b827b2f))

## [2.20.1](https://gitlab.com/toegang-voor-iedereen/pdf/apps/kimi/compare/2.20.0...2.20.1) (2024-11-12)


### Bug Fixes

* antiAffinity and labels ([6011b2c](https://gitlab.com/toegang-voor-iedereen/pdf/apps/kimi/commit/6011b2c840b2ea9a49ce6b824ae67fd0b3418c66))

# [2.20.0](https://gitlab.com/toegang-voor-iedereen/pdf/apps/kimi/compare/2.19.0...2.20.0) (2024-11-12)


### Features

* stations v4.5.0 ([7b6251c](https://gitlab.com/toegang-voor-iedereen/pdf/apps/kimi/commit/7b6251cb4f8cc73512a3efef8e33f20df4b9c79b))

# [2.19.0](https://gitlab.com/toegang-voor-iedereen/pdf/apps/kimi/compare/2.18.0...2.19.0) (2024-11-12)


### Features

* document-output-nldocspec v1.3.0, adding spaces and combining texts ([c420d9d](https://gitlab.com/toegang-voor-iedereen/pdf/apps/kimi/commit/c420d9d86457d6d4a60a78842828521ec10bd07a))

# [2.18.0](https://gitlab.com/toegang-voor-iedereen/pdf/apps/kimi/compare/2.17.0...2.18.0) (2024-11-12)


### Features

* document-output-nldocspec v1.2.1 using specified children and assets ([f3528c9](https://gitlab.com/toegang-voor-iedereen/pdf/apps/kimi/commit/f3528c978953cd3ac26edc83c487b560c956d345))

# [2.17.0](https://gitlab.com/toegang-voor-iedereen/pdf/apps/kimi/compare/2.16.0...2.17.0) (2024-11-12)


### Features

* document-output-nldocspec v1.1.2 supporting Assets ([636e468](https://gitlab.com/toegang-voor-iedereen/pdf/apps/kimi/commit/636e46810b1c1f91ae8dfe704dc2fa19569d393d))

# [2.16.0](https://gitlab.com/toegang-voor-iedereen/pdf/apps/kimi/compare/2.15.0...2.16.0) (2024-11-12)


### Features

* document-output-nldocspec v1.1.1 ([39bf7f8](https://gitlab.com/toegang-voor-iedereen/pdf/apps/kimi/commit/39bf7f8ab68906c370a3d4af14a060bf237c9f2f))

# [2.15.0](https://gitlab.com/toegang-voor-iedereen/pdf/apps/kimi/compare/2.14.1...2.15.0) (2024-11-11)


### Features

* document-output-nldocspec v1.0.2 ([723999b](https://gitlab.com/toegang-voor-iedereen/pdf/apps/kimi/commit/723999b4c7b8b555ab22e9c3362cc5971310ae4a))

## [2.14.1](https://gitlab.com/toegang-voor-iedereen/pdf/apps/kimi/compare/2.14.0...2.14.1) (2024-11-11)


### Bug Fixes

* force update release ([e85ad7c](https://gitlab.com/toegang-voor-iedereen/pdf/apps/kimi/commit/e85ad7ce18454d0e3cfaaf34a1879e3bd13fcbe7))

# [2.14.0](https://gitlab.com/toegang-voor-iedereen/pdf/apps/kimi/compare/2.13.0...2.14.0) (2024-11-11)


### Features

* document-output-nldocspec v1.0.1 ([d0db69d](https://gitlab.com/toegang-voor-iedereen/pdf/apps/kimi/commit/d0db69df087d68e971d8a90ac362eb0e90c1e96e))

# [2.13.0](https://gitlab.com/toegang-voor-iedereen/pdf/apps/kimi/compare/2.12.0...2.13.0) (2024-11-11)


### Features

* removed HTML output worker ([2d58799](https://gitlab.com/toegang-voor-iedereen/pdf/apps/kimi/commit/2d58799da06d067e2834c1d9d99fb3ac8d5bee9e))

# [2.12.0](https://gitlab.com/toegang-voor-iedereen/pdf/apps/kimi/compare/2.11.0...2.12.0) (2024-11-11)


### Features

* added nldocspec output worker ([9fb6ea1](https://gitlab.com/toegang-voor-iedereen/pdf/apps/kimi/commit/9fb6ea1c0b1ec92666ac0ec17667d00c4401506a))

# [2.11.0](https://gitlab.com/toegang-voor-iedereen/pdf/apps/kimi/compare/2.10.0...2.11.0) (2024-11-11)


### Features

* streaming ([4861594](https://gitlab.com/toegang-voor-iedereen/pdf/apps/kimi/commit/4861594662df5e48ed75d4dc7dd44e8b4d10d92e))

# [2.10.0](https://gitlab.com/toegang-voor-iedereen/pdf/apps/kimi/compare/2.9.0...2.10.0) (2024-11-06)


### Features

* station version 4.2.5 with component ([6a6702b](https://gitlab.com/toegang-voor-iedereen/pdf/apps/kimi/commit/6a6702b37bf849d07a1524ecf443019beaa88e6a))
* use station 4.3.0 to use correct exchange name for document-output ([d10482f](https://gitlab.com/toegang-voor-iedereen/pdf/apps/kimi/commit/d10482fb006353379c5bba425300a3e9ae17d67c))

# [2.9.0](https://gitlab.com/toegang-voor-iedereen/pdf/apps/kimi/compare/2.8.0...2.9.0) (2024-11-06)


### Features

* use yolov8 1.9.1-alpha.1 to use ContentDefinition ([6b989cc](https://gitlab.com/toegang-voor-iedereen/pdf/apps/kimi/commit/6b989cc2aa7951726214fecc5a4d51c432db127e))

# [2.8.0](https://gitlab.com/toegang-voor-iedereen/pdf/apps/kimi/compare/2.7.0...2.8.0) (2024-11-05)


### Features

* ocr 1.9.1-alpha.1 ([bc8b2f6](https://gitlab.com/toegang-voor-iedereen/pdf/apps/kimi/commit/bc8b2f6e7db988b82805f2c5aff8aa470699ad25))

# [2.7.0](https://gitlab.com/toegang-voor-iedereen/pdf/apps/kimi/compare/2.6.0...2.7.0) (2024-11-05)


### Features

* use page-content-ocr 1.11.0-alpha.2 ([2f86107](https://gitlab.com/toegang-voor-iedereen/pdf/apps/kimi/commit/2f86107cefad8940e530084364cc62da88d785a2))

# [2.6.0](https://gitlab.com/toegang-voor-iedereen/pdf/apps/kimi/compare/2.5.0...2.6.0) (2024-11-05)


### Features

* use page-content v1.13.0-alpha.2 ([d01d951](https://gitlab.com/toegang-voor-iedereen/pdf/apps/kimi/commit/d01d95108fc35ba307204b0965d7f4f414d166e8))

# [2.5.0](https://gitlab.com/toegang-voor-iedereen/pdf/apps/kimi/compare/2.4.0...2.5.0) (2024-11-05)


### Features

* all workers upgraded to hotfix/alpha version using old ContentDefinition but with HTTPS for MinIO ([baaf92e](https://gitlab.com/toegang-voor-iedereen/pdf/apps/kimi/commit/baaf92ec39506c8572b0610ab4ab01ba7a1e0fa8))

# [2.4.0](https://gitlab.com/toegang-voor-iedereen/pdf/apps/kimi/compare/2.3.1...2.4.0) (2024-11-05)


### Features

* use metadata worker 1.8.0-alpha.1 ([a9f3649](https://gitlab.com/toegang-voor-iedereen/pdf/apps/kimi/commit/a9f3649557a45a2882878a156025b679344e2e83))

## [2.3.1](https://gitlab.com/toegang-voor-iedereen/pdf/apps/kimi/compare/2.3.0...2.3.1) (2024-11-05)


### Bug Fixes

* use correct access mode ([e7f3228](https://gitlab.com/toegang-voor-iedereen/pdf/apps/kimi/commit/e7f3228bcf0ff1f41383b87f1b5f36794cfaad57))

# [2.3.0](https://gitlab.com/toegang-voor-iedereen/pdf/apps/kimi/compare/2.2.2...2.3.0) (2024-11-05)


### Features

* updated document-mimetype worker to kimiworker 1.12.0-alpha.1 so it connects to MinIO over https ([611769a](https://gitlab.com/toegang-voor-iedereen/pdf/apps/kimi/commit/611769ab9826fc41131438f4b6a04f4e467bf172))

## [2.2.2](https://gitlab.com/toegang-voor-iedereen/pdf/apps/kimi/compare/2.2.1...2.2.2) (2024-11-04)


### Bug Fixes

* configured correct exchange names for all workers ([da33983](https://gitlab.com/toegang-voor-iedereen/pdf/apps/kimi/commit/da3398374a0078e21f13509612dbe4ece5f1e9f3))

## [2.2.1](https://gitlab.com/toegang-voor-iedereen/pdf/apps/kimi/compare/2.2.0...2.2.1) (2024-10-31)


### Bug Fixes

* invalid yaml serviceaccount ([dee2b81](https://gitlab.com/toegang-voor-iedereen/pdf/apps/kimi/commit/dee2b811255708328dff06727d5d335d15d640a9))

# [2.2.0](https://gitlab.com/toegang-voor-iedereen/pdf/apps/kimi/compare/2.1.1...2.2.0) (2024-10-31)


### Features

* downgraded all workers to latest ContentDefinition version ([d12f25a](https://gitlab.com/toegang-voor-iedereen/pdf/apps/kimi/commit/d12f25a712aa11b548d426e96865268692153d74))

## [2.1.1](https://gitlab.com/toegang-voor-iedereen/pdf/apps/kimi/compare/2.1.0...2.1.1) (2024-10-28)


### Bug Fixes

* no persistency for workers ([98362ce](https://gitlab.com/toegang-voor-iedereen/pdf/apps/kimi/commit/98362ce9059703c3d122fed5369dd887b460db3f))

# [2.1.0](https://gitlab.com/toegang-voor-iedereen/pdf/apps/kimi/compare/2.0.6...2.1.0) (2024-10-28)


### Bug Fixes

* quoting around values ([a306309](https://gitlab.com/toegang-voor-iedereen/pdf/apps/kimi/commit/a306309de7d3baeba2d2ddec696b4704f0666521))


### Features

* worker functionality ([844cf19](https://gitlab.com/toegang-voor-iedereen/pdf/apps/kimi/commit/844cf1972ae3ef655c7f228ed0c06da6c5b4d57d))

## [2.0.6](https://gitlab.com/toegang-voor-iedereen/pdf/apps/kimi/compare/2.0.5...2.0.6) (2024-10-28)


### Bug Fixes

* correct env var name ([287e3f3](https://gitlab.com/toegang-voor-iedereen/pdf/apps/kimi/commit/287e3f3d6aba0d18e59d2d4437292ea9471c0f0b))

## [2.0.5](https://gitlab.com/toegang-voor-iedereen/pdf/apps/kimi/compare/2.0.4...2.0.5) (2024-10-28)


### Bug Fixes

* remove initContainer ([4bda88b](https://gitlab.com/toegang-voor-iedereen/pdf/apps/kimi/commit/4bda88bae43e92b2a87ae78f2be077968f77ed5d))

## [2.0.4](https://gitlab.com/toegang-voor-iedereen/pdf/apps/kimi/compare/2.0.3...2.0.4) (2024-10-28)


### Bug Fixes

* different name for PodDisruptionBudget ([df22e65](https://gitlab.com/toegang-voor-iedereen/pdf/apps/kimi/commit/df22e6508fa2e684255ed2294a4ae85a80e6b805))

## [2.0.3](https://gitlab.com/toegang-voor-iedereen/pdf/apps/kimi/compare/2.0.2...2.0.3) (2024-10-28)


### Bug Fixes

* add some properties to the initContainer ([615ea25](https://gitlab.com/toegang-voor-iedereen/pdf/apps/kimi/commit/615ea2570e5e4b0f61199c36d037552b68dbc923))

## [2.0.2](https://gitlab.com/toegang-voor-iedereen/pdf/apps/kimi/compare/2.0.1...2.0.2) (2024-10-28)


### Bug Fixes

* remove probes ([e7c7216](https://gitlab.com/toegang-voor-iedereen/pdf/apps/kimi/commit/e7c7216edaf3207be3e2eccc3c6fa4f9b8693057))

## [2.0.1](https://gitlab.com/toegang-voor-iedereen/pdf/apps/kimi/compare/2.0.0...2.0.1) (2024-10-28)


### Bug Fixes

* name of components ([97d9567](https://gitlab.com/toegang-voor-iedereen/pdf/apps/kimi/commit/97d95674146b153ad31d0bf2e672b370966050ba))

# [2.0.0](https://gitlab.com/toegang-voor-iedereen/pdf/apps/kimi/compare/1.11.1...2.0.0) (2024-10-28)


* feat!: rewrite chart ([9d04b6c](https://gitlab.com/toegang-voor-iedereen/pdf/apps/kimi/commit/9d04b6c1802858dfac48e2ceb99077ed45af5804))


### BREAKING CHANGES

* complete rewrite

## [1.11.1](https://gitlab.com/toegang-voor-iedereen/pdf/apps/kimi/compare/1.11.0...1.11.1) (2024-10-23)


### Bug Fixes

* **deps:** helm dependency update ([21d8bda](https://gitlab.com/toegang-voor-iedereen/pdf/apps/kimi/commit/21d8bdab585a74dbff0d80a81247a23ff4e0375d))

# [1.11.0](https://gitlab.com/toegang-voor-iedereen/pdf/apps/kimi/compare/1.10.3...1.11.0) (2024-10-23)


### Features

* NLD-118 options for enabling / disabling rabbitmq and minio ([a17906d](https://gitlab.com/toegang-voor-iedereen/pdf/apps/kimi/commit/a17906dbb24460614d9a87c8a5a1acc73014f043))

## [1.10.3](https://gitlab.com/toegang-voor-iedereen/pdf/apps/kimi/compare/1.10.2...1.10.3) (2024-09-10)


### Bug Fixes

* updated indentation and added ingressClassName ([3d22036](https://gitlab.com/toegang-voor-iedereen/pdf/apps/kimi/commit/3d22036c8b9de8c26462c1759fab4b3018787754))

## [1.10.2](https://gitlab.com/toegang-voor-iedereen/pdf/apps/kimi/compare/1.10.1...1.10.2) (2024-09-10)


### Bug Fixes

* correct common name for minio ([7780ae7](https://gitlab.com/toegang-voor-iedereen/pdf/apps/kimi/commit/7780ae7040f0a0596e159544760490558470458a))

## [1.10.1](https://gitlab.com/toegang-voor-iedereen/pdf/apps/kimi/compare/1.10.0...1.10.1) (2024-09-09)


### Bug Fixes

* correct hostnames for amqp and minio ([9b162de](https://gitlab.com/toegang-voor-iedereen/pdf/apps/kimi/commit/9b162de817d382bbc66d2a570ff0902b7cdfe59a))

# [1.10.0](https://gitlab.com/toegang-voor-iedereen/pdf/apps/kimi/compare/1.9.0...1.10.0) (2024-09-09)


### Bug Fixes

* removed headers from http security policy ([4b222f9](https://gitlab.com/toegang-voor-iedereen/pdf/apps/kimi/commit/4b222f98e498ea4df77cc18eae5feced545f4762))


### Features

* added minio ingress ([a749208](https://gitlab.com/toegang-voor-iedereen/pdf/apps/kimi/commit/a7492088361a143eab09e7165b442a81cffce24f))

# [1.9.0](https://gitlab.com/toegang-voor-iedereen/pdf/apps/kimi/compare/1.8.1...1.9.0) (2024-09-09)


### Features

* added ingress to amqp chart ([4bafd8f](https://gitlab.com/toegang-voor-iedereen/pdf/apps/kimi/commit/4bafd8f6c9a79ca789f6bd30fa6d1abbf3f5a349))

## [1.8.1](https://gitlab.com/toegang-voor-iedereen/pdf/apps/kimi/compare/1.8.0...1.8.1) (2024-09-05)


### Bug Fixes

* use stations 4.2.4, added resource limits and requests to initContainers ([c9ffff0](https://gitlab.com/toegang-voor-iedereen/pdf/apps/kimi/commit/c9ffff0b40bd30e9e234524e72050f7d724bddf8))

# [1.8.0](https://gitlab.com/toegang-voor-iedereen/pdf/apps/kimi/compare/1.7.0...1.8.0) (2024-09-05)


### Features

* upgraded all workers and stations to latest versions ([fab438c](https://gitlab.com/toegang-voor-iedereen/pdf/apps/kimi/commit/fab438ce5ca31de2fe09b75caa4a786703c82279))

# [1.7.0](https://gitlab.com/toegang-voor-iedereen/pdf/apps/kimi/compare/1.6.0...1.7.0) (2024-09-04)


### Features

* stations to v4.2.0 ([c2e882d](https://gitlab.com/toegang-voor-iedereen/pdf/apps/kimi/commit/c2e882defbca10b3acf545469ca609ee81ddf208))
* upgraded all stations and workers to contain new baseworker and correct attributes for containers ([453f030](https://gitlab.com/toegang-voor-iedereen/pdf/apps/kimi/commit/453f030de6c9bae35ab43b011c037fb39a9b6093))

# [1.6.0](https://gitlab.com/toegang-voor-iedereen/pdf/apps/kimi/compare/1.5.0...1.6.0) (2024-09-04)


### Features

* added securityContext to initContainers ([508ae7e](https://gitlab.com/toegang-voor-iedereen/pdf/apps/kimi/commit/508ae7ed7137b70b0d8e727f3fbc6e72407532b9))

# [1.5.0](https://gitlab.com/toegang-voor-iedereen/pdf/apps/kimi/compare/1.4.0...1.5.0) (2024-09-04)


### Features

* force new release with child dependencies ([4b3c44b](https://gitlab.com/toegang-voor-iedereen/pdf/apps/kimi/commit/4b3c44b6f635b6e14944643bd3b7a3fdf6fe679c))

# [1.4.0](https://gitlab.com/toegang-voor-iedereen/pdf/apps/kimi/compare/1.3.0...1.4.0) (2024-09-04)


### Features

* removed filebeat ([5866624](https://gitlab.com/toegang-voor-iedereen/pdf/apps/kimi/commit/5866624bba8df477a1bba6628d655aed135957f1))

# [1.3.0](https://gitlab.com/toegang-voor-iedereen/pdf/apps/kimi/compare/1.2.0...1.3.0) (2024-09-04)


### Features

* publish as helm package ([1561811](https://gitlab.com/toegang-voor-iedereen/pdf/apps/kimi/commit/15618116ed5284b59ae02bb1467ffbaf5fafff3d))

# [1.2.0](https://gitlab.com/toegang-voor-iedereen/pdf/apps/kimi/compare/1.1.2...1.2.0) (2024-08-28)


### Features

* use station v4.1.0 ([251fbd3](https://gitlab.com/toegang-voor-iedereen/pdf/apps/kimi/commit/251fbd3ed41f68e7651034b27bb29f9c7d0ded28))

## [1.1.2](https://gitlab.com/toegang-voor-iedereen/pdf/apps/kimi/compare/1.1.1...1.1.2) (2024-07-11)


### Bug Fixes

* use correct exchange names for workers ([b0e156a](https://gitlab.com/toegang-voor-iedereen/pdf/apps/kimi/commit/b0e156aaf79a3a82e77930d421dace7bff9326fc))

## [1.1.1](https://gitlab.com/toegang-voor-iedereen/pdf/apps/kimi/compare/1.1.0...1.1.1) (2024-07-09)


### Bug Fixes

* correct input type ([5049cd6](https://gitlab.com/toegang-voor-iedereen/pdf/apps/kimi/commit/5049cd6ee49bd7500c66d00bba810b9815eea4c2))
* limit resources ([3b2623b](https://gitlab.com/toegang-voor-iedereen/pdf/apps/kimi/commit/3b2623b0ab411ada74ef80a217bf8394e8e75c72))

# [1.1.0](https://gitlab.com/toegang-voor-iedereen/pdf/apps/kimi/compare/1.0.2...1.1.0) (2024-07-09)


### Features

* add station-page-content ([912f1a9](https://gitlab.com/toegang-voor-iedereen/pdf/apps/kimi/commit/912f1a93cf0a59517596a672083fb65935160e85))

## [1.0.2](https://gitlab.com/toegang-voor-iedereen/pdf/apps/kimi/compare/1.0.1...1.0.2) (2024-07-02)


### Bug Fixes

* readme ([73fe4ba](https://gitlab.com/toegang-voor-iedereen/pdf/apps/kimi/commit/73fe4ba5c303f0b0224852a1f73edb2edbf1c83f))

## [1.0.1](https://gitlab.com/toegang-voor-iedereen/pdf/apps/kimi/compare/1.0.0...1.0.1) (2024-06-24)


### Bug Fixes

* correct input for station-file-document ([723bb24](https://gitlab.com/toegang-voor-iedereen/pdf/apps/kimi/commit/723bb24d9fce31acdd44f93eb8af80945ac2a161))

# 1.0.0 (2024-06-17)


### Bug Fixes

* set update_group_readme needs to release instead of test ([034e31a](https://gitlab.com/toegang-voor-iedereen/pdf/apps/kimi/commit/034e31a45c6af5bd0b1fe42b3a2480e3b82dab15))


### Features

* gitlab ci configuration ([a75a6e3](https://gitlab.com/toegang-voor-iedereen/pdf/apps/kimi/commit/a75a6e3b18330556034414d7cb421091082f8220))
